//
//  PSConnectionView.h
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PSConnectionView;

@protocol PSConnectionViewDelegate <NSObject>
@optional
- (void)psConnectionView:(PSConnectionView*)psConnectionView onButtonClicked:(PSConnectionModel *)connectionModel;
- (void)psConnectionView:(PSConnectionView*)psConnectionView onEnterPassword:(PSConnectionModel *)connectionModel;
@end

@interface PSConnectionView : UIView

@property (nonatomic, strong) IBOutlet UIImageView *thumbnail;
@property (nonatomic, strong) IBOutlet UIImageView *badge;
@property (nonatomic, strong) IBOutlet UILabel *title;
@property (nonatomic, strong) IBOutlet UITextField *password;
@property (nonatomic, strong) IBOutlet UIButton *button;

@property (nonatomic, strong) PSConnectionModel *connectionModel;
@property (nonatomic, unsafe_unretained) id delegate;

- (BOOL)containService:(NSNetService*)aNetService;

- (void)showPassword;
- (void)hidePassword;
- (void)showErrorPassword;

@end
