//
//  PSConnectionView.m
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "PSConnectionView.h"
#import <QuartzCore/QuartzCore.h>

@interface PSConnectionView() <UITextFieldDelegate>
@end

@implementation PSConnectionView
@synthesize thumbnail, badge, title, password, button;
@synthesize connectionModel = _connectionModel;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (!self)
    return self;
   
  return self;
}

- (void)setConnectionModel:(PSConnectionModel *)aConnectionModel
{
  _connectionModel = aConnectionModel;
  self.password.alpha = 0;
  self.badge.alpha = 0;
  
  self.layer.masksToBounds = NO;
  self.clipsToBounds = NO;
  
  float shadowRadius = 10;
  self.title.layer.shadowRadius = shadowRadius;
  self.title.layer.shadowOffset = CGSizeZero;
  self.title.layer.shadowOpacity = 0;
  self.title.layer.masksToBounds = NO;
  self.thumbnail.layer.shadowRadius = shadowRadius;
  self.thumbnail.layer.shadowOffset = CGSizeZero;
  self.thumbnail.layer.shadowOpacity = 0;
  self.thumbnail.layer.masksToBounds = NO;
  self.password.layer.shadowRadius = shadowRadius;
  self.password.layer.shadowOffset = CGSizeZero;
  self.password.layer.shadowOpacity = 0;
  self.password.layer.masksToBounds = NO;
  
  //This should not happen
  if (self.connectionModel == nil)
    return;

  NSString *username = @"";
  NSString *thePassword = @"";
  if (aConnectionModel.serverPassword)
    thePassword = aConnectionModel.serverPassword;
  
  NSNetService *theService = aConnectionModel.serviceObject;
  BOOL bonjourConnection = aConnectionModel.serviceObject != nil || aConnectionModel.serverAddress == nil;
  if (bonjourConnection)    username = theService.name;
  else                      username = aConnectionModel.serverAddress;
  
  self.title.text = username == nil ? @"" : username;
  self.password.text = thePassword;
  
  //Align text to middle
  CGSize actualTextSize = [self.title.text sizeWithFont:self.title.font forWidth:self.title.frame.size.width lineBreakMode:self.title.lineBreakMode];
  CGRect titleFrame;
  titleFrame.origin.x = self.title.frame.origin.x;
  titleFrame.origin.y = self.bounds.size.height/2 - actualTextSize.height/2;
  titleFrame.size = actualTextSize;
  self.title.frame = titleFrame;
}



#pragma mark - Actions

- (IBAction)onButtonPressed:(id)sender
{
  float opacity = 0.7;
  self.title.layer.shadowOpacity = opacity;
  self.thumbnail.layer.shadowOpacity = opacity;
  self.password.layer.shadowOpacity = opacity;
}

- (IBAction)onButtonReleased:(id)sender
{
  self.title.layer.shadowOpacity = 0;
  self.thumbnail.layer.shadowOpacity = 0;
  self.password.layer.shadowOpacity = 0;
}

- (IBAction)onButtonClicked:(id)sender
{
  [self onButtonPressed:sender];
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.15 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
    [self onButtonReleased:sender];
  });
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.18 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
    
    //Let the controller decide on what to do next
    if ([self.delegate respondsToSelector:@selector(psConnectionView:onButtonClicked:)])
      [self.delegate psConnectionView:self onButtonClicked:self.connectionModel];   
    
  });
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
  self.connectionModel.serverPassword = nil;
  if ([self.password.text length] < MIN_PASSWORD_LENGTH) {
    [self.password shakeX];
    return NO;
  }
  
  self.connectionModel.serverPassword = self.password.text;
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
    
    //Let the controller decide on what to do next
    if ([self.delegate respondsToSelector:@selector(psConnectionView:onEnterPassword:)])
      [self.delegate psConnectionView:self onEnterPassword:self.connectionModel];
      
  });

  return YES;
}



#pragma mark - UI helpers

- (BOOL)containService:(NSNetService*)aNetService
{
  if (self.connectionModel == nil || self.connectionModel.serviceObject == nil)
    return NO;
  return [self.connectionModel.serviceObject isEqual:aNetService];
}

- (void)showPassword
{
  CGRect titleFrame = self.title.frame;
  titleFrame.origin.y = 4;
  
  [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
    self.title.frame = titleFrame;
  } completion:^(BOOL finished) {
    [self.password becomeFirstResponder];
  }];
  
  [UIView animateWithDuration:0.2 delay:0.1 options:UIViewAnimationCurveEaseInOut animations:^{
    self.password.alpha = 1;
    self.badge.alpha = 1;
  } completion:^(BOOL finished) {
    
  }];
}

- (void)hidePassword
{
  self.password.delegate = nil;
  [self.password resignFirstResponder];
  self.password.delegate = self;
  
  CGRect titleFrame = self.title.frame;
  titleFrame.origin.y = self.bounds.size.height/2 - titleFrame.size.height/2;

  [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
    self.password.alpha = 0;
    self.badge.alpha = 0;
  } completion:^(BOOL finished) {
    
  }];
  
  [UIView animateWithDuration:0.2 delay:0.1 options:UIViewAnimationCurveEaseInOut animations:^{
    self.title.frame = titleFrame;
  } completion:^(BOOL finished) {
    
  }];
}

- (void)showErrorPassword
{
  self.password.delegate = nil;
  [self showPassword];
  [self.password shakeX];
  self.password.delegate = self;
}

@end
