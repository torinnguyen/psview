//
//  TNViewController.m
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "PSSplashViewController.h"
#import "PSConnectionViewController.h"
#import "PSRemoteViewController.h"

@interface PSSplashViewController ()

@end

@implementation PSSplashViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
  
  //Show connection UI
  PSConnectionModel *activeConnection = [PSSharedConnection sharedLibrary].lastConnectionModel;
  if (activeConnection == nil) {
    PSConnectionViewController *vc = [[PSConnectionViewController alloc] initWithNibName:@"PSConnectionViewController" bundle:nil];
    [self presentModalViewController:vc animated:NO];
    return;
  }
  
  //Show remote view
  PSRemoteViewController *vc = [[PSRemoteViewController alloc] initWithNibName:@"PSRemoteViewController" bundle:nil];
  [self presentModalViewController:vc animated:NO];
  return;
}

- (void)viewDidUnload
{
  [super viewDidUnload];
}

@end
