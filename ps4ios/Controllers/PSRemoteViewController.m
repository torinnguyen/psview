//
//  PSRemoteViewController.m
//  ps4ios
//
//  Created by Torin Nguyen on 25/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "PSRemoteViewController.h"

@interface PSRemoteViewController() <PSSharedConnectionDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIImageView *theImageView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *theIndicator;

@property (nonatomic, strong) IBOutlet UIView *menuView;
@property (nonatomic, strong) IBOutlet UIView *menuViewBackground;
@property (nonatomic, strong) IBOutlet UIButton *btnExit;
@property (nonatomic, strong) IBOutlet UIButton *btnPin;
@property (nonatomic, strong) IBOutlet UIButton *btnSize11;
@property (nonatomic, strong) IBOutlet UIButton *btnSize12;
@property (nonatomic, strong) IBOutlet UIButton *btnSizeFullscreen;
@property (nonatomic, strong) IBOutlet UIButton *btnMore;
@property (nonatomic, strong) IBOutlet UIButton *btnModeJPEG;
@property (nonatomic, strong) IBOutlet UIButton *btnModePNG;
@property (nonatomic, strong) IBOutlet UIView *foregroundColorView;
@property (nonatomic, strong) IBOutlet UIView *backgroundColorView;
@property (nonatomic, strong) IBOutlet UILabel *lblInstruction;
@property (nonatomic, strong) UIImage *currentImage;

@end

@implementation PSRemoteViewController
@synthesize scrollView;
@synthesize theImageView;
@synthesize theIndicator;
@synthesize menuView, menuViewBackground, btnExit, btnPin, btnMore;
@synthesize btnSize11, btnSize12, btnSizeFullscreen;
@synthesize btnModeJPEG, btnModePNG;
@synthesize foregroundColorView, backgroundColorView, lblInstruction;

@synthesize currentImage;

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.theImageView.autoresizingMask = UIViewAutoresizingNone;
  [self.theIndicator startAnimating];
  self.lblInstruction.text = @"";
  
  //Round corners for background color buttons
  for (UIButton *button in self.menuView.subviews) {
    if (![button isKindOfClass:[UIButton class]])
      continue;
    if (button.tag <= 0 || button.tag >= kSize11Tag)
      continue;
    [button setTitle:@"" forState:UIControlStateNormal];
    button.backgroundColor = [self getBackgroundColorForTag:button.tag];
    button.layer.cornerRadius = 8;
    button.layer.masksToBounds = YES;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor grayColor].CGColor;
  }
  
  //Make it fast
  self.menuView.layer.shadowOpacity = 0.4;
  self.menuView.layer.shadowRadius = 10;
  self.menuView.layer.shouldRasterize = YES;
  self.menuView.layer.rasterizationScale = [UIScreen mainScreen].scale;
  
  //Foreground, background colors
  self.foregroundColorView.layer.borderColor = [UIColor whiteColor].CGColor;
  self.foregroundColorView.layer.borderWidth = 6;
  self.foregroundColorView.layer.shadowOpacity = 0.5;
  self.foregroundColorView.layer.shadowRadius = 6;
  self.backgroundColorView.layer.borderColor = [UIColor whiteColor].CGColor;
  self.backgroundColorView.layer.borderWidth = 6;
  self.backgroundColorView.layer.shadowOpacity = 0.5;
  self.backgroundColorView.layer.shadowRadius = 6;
  [self updateColors];
  
  //From NSUserDefaults
  [self applyRememberedSettings];

  //Show half menu initially
  [self hideMenu:NO];
  [self showHalfMenu:YES];
  if (!self.btnPin.selected) {
    self.menuView.userInteractionEnabled = NO;
    [self performSelector:@selector(hideMenuAnimated) withObject:nil afterDelay:1.1];
  }
  
  //Single tap gesture recognizer
  UITapGestureRecognizer *tapGestureRecognize = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onSingleTapGestureRecognizer:)];
  tapGestureRecognize.delegate = self;
  tapGestureRecognize.numberOfTapsRequired = 1;
  [self.view addGestureRecognizer:tapGestureRecognize];  
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];

  [self checkValidImage];
  
  [PSSharedConnection sharedLibrary].delegate = self;
  [[PSSharedConnection sharedLibrary] setImageRefreshDuration:1];
  [[PSSharedConnection sharedLibrary] startReceivingImages];
  
  //Fade in
  self.view.alpha = 0;
  [UIView animateWithDuration:1.0 animations:^{
    self.view.alpha = 1;
  }];
  
  //Prevent screen autolock
  [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{  
  [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
  
  [super viewWillDisappear:animated];
}


#pragma mark - UI setup

- (void)checkValidImage
{
  //Has valid image
  UIImage *lastImage = [PSSharedConnection sharedLibrary].lastImage;
  if (lastImage != nil) {
    self.currentImage = lastImage;
    [self updateCurrentImageView];
    return;
  }
  
  [self updateBackground:kBluePrintBgTag];
  [self.theIndicator stopAnimating];
  self.lblInstruction.alpha = 0;
  self.lblInstruction.text = @"To get started, create a new document or open an existing file in Photoshop";
  [UIView animateWithDuration:0.3 animations:^{
    self.lblInstruction.alpha = 1;
  }];
}

- (void)applyRememberedSettings
{
  [self updateMode:[[PSSharedData sharedLibrary] getSettingJPEG]];
  self.btnPin.selected = [[PSSharedData sharedLibrary] getSettingPin];
  int sizeTag = [[PSSharedData sharedLibrary] getSettingSize];
  if (sizeTag > 100)     [self updateSize:sizeTag animated:NO];
  int bgTag = [[PSSharedData sharedLibrary] getSettingBackground];
  if (bgTag > 0)         [self updateBackground:bgTag];
}

#pragma mark - UI helpers

- (void)updateColors
{
  self.foregroundColorView.backgroundColor = [PSSharedConnection sharedLibrary].lastForegroundColor;
  self.backgroundColorView.backgroundColor = [PSSharedConnection sharedLibrary].lastBackgroundColor;
}

- (void)updateCurrentImageView
{
  BOOL firstTime = self.theImageView.image == nil;
  
  [self updateSize:[self getCurrentSizeTag] animated:NO];  
  self.theImageView.image = self.currentImage;
  
  if (!firstTime)
    return;

  int bgTag = [[PSSharedData sharedLibrary] getSettingBackground];
  if (bgTag > 0)         [self updateBackground:bgTag];

  [self hideMenu:YES];
  [self.theIndicator stopAnimating];
  self.theImageView.alpha = 0;
  
  [UIView animateWithDuration:0.5 animations:^{
    self.theImageView.alpha = 1;
  
  } completion:^(BOOL finished) {
    self.lblInstruction.text = @"";
    self.foregroundColorView.alpha = 0;
    self.backgroundColorView.alpha = 0;
  }];  
}

- (int)getCurrentSizeTag
{
  if (self.btnSize11.selected)          return kSize11Tag;
  if (self.btnSize12.selected)          return kSize12Tag;
  if (self.btnSizeFullscreen.selected)  return kSizeFSTag;
  return 1;
}

- (void)updateSize:(int)tag animated:(BOOL)animated
{
  if (tag < kSize11Tag)
    tag += kSize11Tag-1;

  self.btnSize11.selected = tag == kSize11Tag;
  self.btnSize12.selected = tag == kSize12Tag;
  self.btnSizeFullscreen.selected = tag == kSizeFSTag;
  
  CGRect imageFrame = CGRectZero;
  CGPoint scrollViewOffset = self.scrollView.contentOffset;
  float deviceScale = [UIScreen mainScreen].scale;
      
  //1:1
  if (tag == kSize11Tag)
  {
    CGRect frame = CGRectZero;
    frame.size.width = self.currentImage.size.width / deviceScale;              //handle retina displays
    frame.size.height = self.currentImage.size.height / deviceScale;
    imageFrame = frame;
    self.theImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    BOOL bigger = imageFrame.size.height > self.view.bounds.size.height || imageFrame.size.width > self.view.bounds.size.width;
    if (bigger)   self.scrollView.contentSize = imageFrame.size;
    else          self.scrollView.contentSize = self.scrollView.bounds.size;
    
    if (bigger) {
      imageFrame.origin.x = 0;
      imageFrame.origin.y = 0;
    }
    else {
      imageFrame.origin.x = self.scrollView.bounds.size.width/2 - imageFrame.size.width/2;
      imageFrame.origin.y = self.scrollView.bounds.size.height/2 - imageFrame.size.height/2;
    }
    if (animated) {
      scrollViewOffset = CGPointZero;
    }
  }
  
  //1:2
  else if (tag == kSize12Tag)
  {
    CGRect frame = self.theImageView.frame;
    frame.size.width = self.currentImage.size.width / 2 / deviceScale;              //handle retina displays
    frame.size.height = self.currentImage.size.height / 2 / deviceScale;
    imageFrame = frame;
    self.theImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    
    BOOL bigger = imageFrame.size.height > self.view.bounds.size.height || imageFrame.size.width > self.view.bounds.size.width;
    if (bigger)   self.scrollView.contentSize = imageFrame.size;
    else          self.scrollView.contentSize = self.scrollView.bounds.size;
    
    if (bigger) {
      imageFrame.origin.x = 0;
      imageFrame.origin.y = 0;
    }
    else {
      imageFrame.origin.x = self.scrollView.bounds.size.width/2 - imageFrame.size.width/2;
      imageFrame.origin.y = self.scrollView.bounds.size.height/2 - imageFrame.size.height/2;
    }
    if (animated) {
      scrollViewOffset = CGPointZero;
    }
  }
  
  //Fullscreen
  else if (tag == kSizeFSTag)
  {
    imageFrame = self.scrollView.bounds;
    self.theImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.scrollView.contentSize = self.scrollView.bounds.size;
    scrollViewOffset = CGPointZero;
  }
  
  if (!animated) {
    self.theImageView.frame = imageFrame;
    self.scrollView.contentOffset = scrollViewOffset;
    return;
  }
  
  //Nice animation
  [UIView animateWithDuration:0.2f delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
    self.theImageView.frame = imageFrame;
    self.scrollView.contentOffset = scrollViewOffset;
  } completion:^(BOOL finished) {
    
  }];
}

- (void)updateBackground:(int)tag
{
  self.view.backgroundColor = [self getBackgroundColorForTag:tag];
  
  //Highlight selected background
  for (UIButton *button in self.menuView.subviews) {
    if (![button isKindOfClass:[UIButton class]])
      continue;
    if (button.tag <= 0 || button.tag >= kSize11Tag)
      continue;
    button.layer.borderWidth = button.tag == tag ? 3 : 1;
    button.layer.borderColor = button.tag == tag ? [UIColor whiteColor].CGColor : [UIColor grayColor].CGColor;
  }
}

- (void)updateMode:(BOOL)isJPEG
{
  self.btnModeJPEG.selected = isJPEG;
  self.btnModePNG.selected = !isJPEG;
  [PSSharedConnection sharedLibrary].isJPEG = isJPEG;
}

- (UIColor*)getBackgroundColorForTag:(int)tag
{ 
  if (tag == 1)
    return [UIColor blackColor];
  if (tag == 2)
    return [UIColor grayColor];
  if (tag == 3)
    return [UIColor whiteColor];
  if (tag == 4)
    return [UIColor redColor];
  if (tag == 5)
    return [UIColor greenColor];
  if (tag == 6)
    return [UIColor scrollViewTexturedBackgroundColor];
  if (tag == 7)
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_checker"]];
  if (tag == kBluePrintBgTag)
    return [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_blueprint"]];
  return [UIColor clearColor];
}

- (void)showHalfMenu:(BOOL)animated
{
  self.btnMore.selected = NO;
  
  CGRect frame = self.menuView.frame;
  BOOL isFull = frame.origin.y < self.view.bounds.size.height - self.btnPin.frame.size.height - 5;
  
  frame.origin.y = self.view.bounds.size.height - self.btnPin.frame.size.height;
  if (!animated) {
    self.menuView.frame = frame;
    self.menuViewBackground.alpha = 0.75;
    return;
  }
  
  //animation
  [UIView animateWithDuration:(isFull ? 0.5f : 0.3f) delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
    self.menuView.frame = frame;
    self.menuViewBackground.alpha = 0.75;
  } completion:^(BOOL finished) {
    
  }];
}

- (void)showFullMenu:(BOOL)animated
{
  self.btnMore.selected = YES;
  
  CGRect frame = self.menuView.frame;
  frame.origin.y = self.view.bounds.size.height - self.menuView.frame.size.height;
  if (!animated) {
    self.menuView.frame = frame;
    self.menuViewBackground.alpha = 0.9;
    return;
  }
  
  //animation
  [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
    self.menuView.frame = frame;
    self.menuViewBackground.alpha = 0.9;
  } completion:^(BOOL finished) {
    
  }];
}

- (void)hideMenu:(BOOL)animated
{
  self.btnMore.selected = NO;
  
  //Always show half menu when:
  // - Pin icon is selected
  // - No valid image
  if (animated && (self.btnPin.selected || self.theImageView.image == nil)) {
    [self showHalfMenu:YES];
    return;
  }
  
  CGRect frame = self.menuView.frame;
  frame.origin.y = self.view.bounds.size.height + 5;
  if (!animated) {
    self.menuView.frame = frame;
    return;
  }
  
  //animation
  [UIView animateWithDuration:0.4f delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
    self.menuView.frame = frame;
  } completion:^(BOOL finished) {
    
  }];
}

- (void)hideMenuAnimated
{
  self.menuView.userInteractionEnabled = YES;
  [self hideMenu:YES];
}

- (void)highlightBtnPin
{
  [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationCurveLinear animations:^{
    self.btnPin.transform = CGAffineTransformMakeScale(1.3, 1.3);
  } completion:^(BOOL finished) {
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationCurveEaseOut animations:^{
      self.btnPin.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
      
    }];   
  }];
}



#pragma mark - Action

- (IBAction)onBtnExit:(id)sender
{
  //Disconnect early
  [PSSharedConnection sharedLibrary].delegate = nil;
  [[PSSharedConnection sharedLibrary] disconnect];
  [PSSharedConnection sharedLibrary].lastConnectionModel = nil;
  
  [UIView animateWithDuration:0.6f animations:^{
    self.view.alpha = 0;
    
  } completion:^(BOOL finished) {
    [self dismissModalViewControllerAnimated:NO];
  }];
}

- (IBAction)onBtnPin:(id)sender
{ 
  CGRect frame = self.menuView.frame;
  BOOL isFull = frame.origin.y < self.view.bounds.size.height - self.btnPin.frame.size.height - 5;

  self.btnPin.selected = !self.btnPin.selected;
  [[PSSharedData sharedLibrary] setSettingPin:self.btnPin.selected];
  
  NSString *selected = self.btnPin.selected ? @"yes" : @"no";
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:selected, FLURRY_PIN_BUTTON, nil];
  [FlurryAnalytics logEvent:FLURRY_PIN_BUTTON withParameters:dictionary];
  
  if (!self.btnPin.selected && !isFull)
    [self hideMenu:YES];
}

- (IBAction)onBtnMore:(id)sender
{
  //Toggle half/full menu view
  CGRect frame = self.menuView.frame;
  BOOL isFull = frame.origin.y < self.view.bounds.size.height - self.btnPin.frame.size.height - 5;
  if (isFull)        [self showHalfMenu:YES];
  else               [self showFullMenu:YES];
  
  NSString *selected = isFull ? @"yes" : @"no";
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:selected, FLURRY_MORE_BUTTON, nil];
  [FlurryAnalytics logEvent:FLURRY_MORE_BUTTON withParameters:dictionary];
}

- (IBAction)onBtnMode:(id)sender
{
  BOOL isJPEG = [sender isEqual:self.btnModeJPEG];
  [self updateMode:isJPEG];
  [[PSSharedData sharedLibrary] setSettingJPEG:isJPEG];
  
  NSString *selected = isJPEG ? @"jpeg" : @"png";
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:selected, FLURRY_FORMAT_BUTTON, nil];
  [FlurryAnalytics logEvent:FLURRY_FORMAT_BUTTON withParameters:dictionary];
}

- (IBAction)onSettingSizeChange:(id)sender
{
  if (![sender isKindOfClass:[UIButton class]])
    return;
  UIButton *button = (UIButton*)sender;
  [[PSSharedData sharedLibrary] setSettingSize:button.tag];
  [self updateSize:button.tag animated:YES];
  
  NSString *size = @"1:1";
  if (button.tag == kSize12Tag)         size = @"1:2";
  else if (button.tag == kSizeFSTag)    size = @"fs";
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:size, FLURRY_SIZE_BUTTON, nil];
  [FlurryAnalytics logEvent:FLURRY_SIZE_BUTTON withParameters:dictionary];
}

- (IBAction)onSettingBackgroundChange:(id)sender
{
  if (![sender isKindOfClass:[UIButton class]])
    return;
  UIButton *button = (UIButton*)sender;
  [[PSSharedData sharedLibrary] setSettingBackground:button.tag];
  [self updateBackground:button.tag];

  NSNumber *bg = [NSNumber numberWithInt:button.tag];
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:bg, FLURRY_BG_COLOR_BUTTON, nil];
  [FlurryAnalytics logEvent:FLURRY_BG_COLOR_BUTTON withParameters:dictionary];
}


#pragma mark - Gesture recognizer

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
  if (touch.view == self.menuView || [self.menuView.subviews containsObject:touch.view])
    return NO;
  if (touch.view == self.btnExit)
    return NO;
  return YES;
}

- (void)onSingleTapGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer
{
  //Toggle show/hide menu view
  CGRect frame = self.menuView.frame;
  BOOL isFull = frame.origin.y < self.view.bounds.size.height - self.btnPin.frame.size.height - 5;
  BOOL isHidden = frame.origin.y > self.view.bounds.size.height;
  
  if (isHidden)                               [self showHalfMenu:YES];
  else if (isFull && !self.btnPin.selected)   [self hideMenu:YES];
  else if (isFull && self.btnPin.selected)    [self showHalfMenu:YES];
  else if (self.btnPin.selected)              [self highlightBtnPin];
  else                                        [self hideMenu:YES];
}



#pragma mark - Background thread

/*
 * This will be fired in background thread, we should switch to main UI thread to update image
 */
- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection receiveImage:(UIImage*)image
{
  [self.theIndicator stopAnimating];
  self.currentImage = image;
  
  dispatch_async(dispatch_get_main_queue(), ^(void) {
    [self updateCurrentImageView];
  });
}

/*
 * The connection could be interuppted if password in Photoshop is changed half way
 */
- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection model:(PSConnectionModel*)model onConnection:(BOOL)success
{
  //Photoshop is closed. Do nothing, keep the state as-is
  if (!psSharedConnection.isBadConnection) {
    [FlurryAnalytics logEvent:FLURRY_DISCONNECTED_LIVE];
    [SVProgressHUD showErrorWithStatus:@"Oops! You got disconnected!"];
    return;
  }

  dispatch_async(dispatch_get_main_queue(), ^(void) {
    [FlurryAnalytics logEvent:FLURRY_CHANGE_PASSWORD_LIVE];
    [SVProgressHUD showErrorWithStatus:@"Oops! Did you just change your password?"];
    [self onBtnExit:self.btnExit];
  });
}

/*
 * Just for fun
 */
- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection foregroundColorChanged:(UIColor*)color
{
  [self updateColors];
}

/*
 * Just for fun
 */
- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection backgroundColorChanged:(UIColor*)color
{
  [self updateColors];
}

@end
