//
//  TNConnectionViewController.m
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "PSConnectionViewController.h"
#import "PSHelpViewController.h"
#import "PSConnectionView.h"

#define kStringAddService           @"Add"
#define kAddServiceUIDuration       0.4f
#define kRemoveServiceUIDuration    0.4f

@interface PSConnectionViewController ()
<
PSSharedDataDelegate,
PSConnectionViewDelegate
>

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet PSConnectionView *focusConnectionView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *indicator;
@property (nonatomic, strong) IBOutlet UILabel *lblStatus;
@property (nonatomic, strong) IBOutlet UIButton *btnHelp;
@property (nonatomic, assign) int numCols;
@property (nonatomic, assign) int waitCounter;
@property (nonatomic, strong) NSDate *connectWaitTime;

- (IBAction)onBtnHelp:(id)sender;

@end

@implementation PSConnectionViewController
@synthesize scrollView;
@synthesize focusConnectionView;
@synthesize indicator;
@synthesize lblStatus;
@synthesize btnHelp;
@synthesize numCols;
@synthesize waitCounter;
@synthesize connectWaitTime;


#pragma mark - View life cycle

- (void)viewDidLoad
{
  [super viewDidLoad];

  self.numCols = 1;
  self.waitCounter = 0;
  
  //Create a new connection to avoid stucked, not needed anymore
  //[PSSharedConnection newConnection];

  NSMutableArray *serviceList = [[PSSharedData sharedLibrary] getServiceList];
  if (serviceList.count <= 0)
    [self startIndicatorSpinning];
  
  //Instruction
  int launchCount = [[PSSharedData sharedLibrary] getLaunchCount];
  if (launchCount <= 0)
    [self.btnHelp setTitle:@"New to PS View?" forState:UIControlStateNormal];
  
  else if (launchCount % 2 == 0)
    [self.btnHelp setTitle:@"Can't find your Photoshop?" forState:UIControlStateNormal];
  
  else 
    [self.btnHelp setTitle:@"Need some help?" forState:UIControlStateNormal];
  
  [[PSSharedData sharedLibrary] increaseLaunchCount];
}

- (void)viewDidUnload
{
  [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
  [center addObserver:self selector:@selector(noticeShowKeyboard:) name:UIKeyboardWillShowNotification object:nil];
  [center addObserver:self selector:@selector(noticeHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
  
  //When app come back from background, there will be notification, but no viewWillAppear
  [self reloadConnectionUI];
}

- (void)viewWillDisappear:(BOOL)animated
{  
  [PSSharedData sharedLibrary].bonjourDelegate = nil;
  [PSSharedConnection sharedLibrary].delegate = nil;
  
  NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
  [center removeObserver:self];
  
  [super viewWillDisappear:animated];
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
  BOOL isLandscape = UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
  self.numCols = isLandscape ? 2 : 1;
  
  NSMutableArray *serviceList = [[PSSharedData sharedLibrary] getServiceList];
  for (NSNetService *aNetService in serviceList)
    [self updateServiceUI:aNetService];
}



#pragma mark - Actions

- (IBAction)onBtnHelp:(id)sender
{
  [self.btnHelp shakeX];
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
    PSHelpViewController *vc = [[PSHelpViewController alloc] initWithNibName:@"PSHelpViewController" bundle:nil];
    [self presentModalViewController:vc animated:NO];
  });
  
  NSNumber *launchCount = [NSNumber numberWithInt:[[PSSharedData sharedLibrary] getLaunchCount]];
  NSNumber *connCount = [NSNumber numberWithInt:[self numServiceUI]];
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:launchCount, FLURRY_LAUNCH_COUNT, connCount, FLURRY_CONNECTION_COUNT, nil];
  [FlurryAnalytics logEvent:FLURRY_HELP_BUTTON withParameters:dictionary];
}

- (void)onAppBecomeActiveNotification:(NSNotification *)notification
{
  [super onAppBecomeActiveNotification:notification];
  [self reloadConnectionUI];
}

- (void)reloadConnectionUI
{
  //Remove all previous UI
  for (UIView *subview in self.scrollView.subviews)
    [subview removeFromSuperview];
  
  //Preload all Bonjour services have been detected so far
  NSMutableArray *serviceList = [[PSSharedData sharedLibrary] getServiceList];
  for (NSNetService *aNetService in serviceList)
    [self addServiceUI:aNetService];
  
  [PSSharedData sharedLibrary].bonjourDelegate = self;
  [[PSSharedData sharedLibrary] stopListening];
  [[PSSharedData sharedLibrary] reset];
  [[PSSharedData sharedLibrary] startListening];
  
  self.lblStatus.alpha = 0;
  self.waitCounter = 5;
  [self performSelector:@selector(updateStatus) withObject:nil afterDelay:self.waitCounter];
}

- (void)noticeShowKeyboard:(NSNotification *)notification
{
  // Get animation info from userInfo
  NSTimeInterval animationDuration;
  UIViewAnimationCurve animationCurve;
  CGRect keyboardEndFrame;
  [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
  [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
  [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
  
  CGRect scrollViewRect = self.scrollView.frame;
  scrollViewRect.size.height = CGRectGetMinY(keyboardEndFrame) - CGRectGetMinY(self.scrollView.frame);
  
  [UIView animateWithDuration:animationDuration delay:0 options:animationCurve animations:^{
    self.scrollView.frame = scrollViewRect;
  } completion:^(BOOL finished) {
    
  }];
  
  //Move the focused UI to center
  if (self.focusConnectionView != nil)
  {
    CGRect focusViewFrame = self.focusConnectionView.frame;  
    CGPoint offset = CGPointMake(0, CGRectGetMidY(focusViewFrame));
    offset.y -= CGRectGetHeight(scrollViewRect) / 2;
    
    if (offset.y < 0)
      offset.y = 0;
    if (offset.y + CGRectGetHeight(scrollViewRect) > self.scrollView.contentSize.height)
      offset.y = self.scrollView.contentSize.height - CGRectGetHeight(scrollViewRect);
      
    [self.scrollView setContentOffset:offset animated:YES];
  }
}

- (void)noticeHideKeyboard:(NSNotification *)notification
{
  // Get animation info from userInfo
  NSTimeInterval animationDuration;
  UIViewAnimationCurve animationCurve;
  CGRect keyboardEndFrame;
  [[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
  [[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
  [[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
  
  CGRect scrollViewRect = self.scrollView.frame;
  scrollViewRect.size.height = CGRectGetMinY(self.btnHelp.frame) - CGRectGetMinY(self.scrollView.frame);

  [UIView animateWithDuration:animationDuration delay:0 options:animationCurve animations:^{
    self.scrollView.frame = scrollViewRect;
  } completion:^(BOOL finished) {
    
  }];
}


#pragma mark - UI helpers

- (void)startIndicatorSpinning
{
  [self.indicator startAnimating];
}

- (void)stopIndicatorSpinning
{
  [self.indicator stopAnimating];
}

- (void)updateStatus
{
  //Found some services
  if ([self numServiceUI] > 0)
  {
    [UIView animateWithDuration:0.5 animations:^{
      self.lblStatus.alpha = 0;
    }];
    return;
  }

  if (self.waitCounter <= 5)
  {
    self.lblStatus.text = @"Searching for Photoshop...";
    [UIView animateWithDuration:0.5 animations:^{
      self.lblStatus.alpha = 1;
    }];
    self.waitCounter = 10;
    [self performSelector:@selector(updateStatus) withObject:nil afterDelay:5];
    return;
  }
  
  if (self.waitCounter <= 25)
    [self.lblStatus shakeX];
  else
    [self.btnHelp shakeX];
  
  if (self.waitCounter <= 10)
  {
    self.lblStatus.text = @"Still searching...";
    self.waitCounter = 15;
    [self performSelector:@selector(updateStatus) withObject:nil afterDelay:5];
    return;
  }
  
  if (self.waitCounter <= 15)
  {
    self.lblStatus.text = @"Hold on...";
    self.waitCounter = 20;
    [self performSelector:@selector(updateStatus) withObject:nil afterDelay:5];
    return;
  }
  
  if (self.waitCounter <= 20)
  {
    self.lblStatus.text = @"Be patient...";
    self.waitCounter = 25;
    [self performSelector:@selector(updateStatus) withObject:nil afterDelay:5];
    return;
  }
  
  if (self.waitCounter <= 25)
  {
    self.lblStatus.text = @"You should really hit the\nHelp link at the bottom!";
    self.waitCounter = 30;
    [self performSelector:@selector(updateStatus) withObject:nil afterDelay:5];
    return;
  }
  
  if (self.waitCounter >= 60)
    return;
  self.waitCounter += 5;
  [self performSelector:@selector(updateStatus) withObject:nil afterDelay:5];
}


#pragma mark - UI helpers - Services

- (int)numServiceUI
{
  int count = 0;
  for (PSConnectionView *subview in self.scrollView.subviews) {
    if (![subview isKindOfClass:[PSConnectionView class]])
      continue;
    count++;
  }
  return count;
}

- (BOOL)hasServiceUI:(NSNetService *)aNetService
{
  return [self getServiceUI:aNetService] != nil;
}

- (PSConnectionView*)getServiceUI:(NSNetService *)aNetService
{
  for (PSConnectionView *subview in self.scrollView.subviews) {
    if (![subview isKindOfClass:[PSConnectionView class]])
      continue;
    if (![subview containService:aNetService])
      continue;
    return subview;
  }
  return nil;
}

- (void)addServiceUI:(NSNetService *)aNetService
{
  if (![self hasServiceUI:aNetService])
  {
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PSConnectionView" owner:nil options:nil];
    PSConnectionView *newView = (PSConnectionView *)[nib objectAtIndex:0];

    //Construct a new model for this connection
    PSConnectionModel *model = [[PSConnectionModel alloc] init];
    model.serverPassword = nil;
    model.serviceObject = aNetService;
    newView.connectionModel = model;
    newView.delegate = self;
    [self.scrollView addSubview:newView];
    
    //Hide it initially
    CGRect frame = newView.frame;
    frame.origin.x = self.scrollView.bounds.size.width+1;
    newView.frame = frame;
  }

  [self updateServiceUI:aNetService];
}

- (void)updateServiceUI:(NSNetService *)aNetService
{
  int index = [[[PSSharedData sharedLibrary] getServiceList] indexOfObject:aNetService];
  if (index < 0)
    return; 
  PSConnectionView *newView = [self getServiceUI:aNetService];
  if (newView == nil)
    return;
  
  BOOL alreadyVisible = newView.frame.origin.x < self.scrollView.bounds.size.width;
  
  int colIndex = index % self.numCols;
  int rowIndex = index / self.numCols;
  int spacingX = (self.scrollView.bounds.size.width - self.numCols*newView.frame.size.width) / (self.numCols+1);
  int spacingY = newView.frame.size.height/2;
  
  CGRect frame = newView.frame;
  frame.origin.x = spacingX + colIndex * (frame.size.width + spacingX);
  frame.origin.y = newView.frame.size.height/2 + rowIndex * (frame.size.height + spacingY);
  
  [self updateScrollViewContentSize];
  
  //Animation
  BOOL isPad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
  float duration = isPad ? 0.5f : kAddServiceUIDuration;
  float delay = frame.origin.y / self.scrollView.bounds.size.height * kAddServiceUIDuration/2;
  
  //Don't pull out if it's already in view
  if (!alreadyVisible) {
    CGRect hideFrame = frame;
    hideFrame.origin.x = self.scrollView.bounds.size.width;
    newView.frame = hideFrame;
  }
  
  [UIView animateWithDuration:duration delay:delay options:UIViewAnimationCurveEaseInOut animations:^{
    newView.frame = frame;
  } completion:^(BOOL finished) {
    
  }];
}

- (void)removeServiceUI:(NSNetService *)aNetService useDelay:(BOOL)useDelay
{
  UIView *subview = nil;
  for (PSConnectionView *aSubview in self.scrollView.subviews) {
    if (![aSubview isKindOfClass:[PSConnectionView class]])
      continue;
    if (![aSubview containService:aNetService])
      continue;
    subview = aSubview;
    break;
  }
  
  if (subview == nil)
    return;
  
  //Animation
  float delay = subview.frame.origin.y / self.view.bounds.size.height * kAddServiceUIDuration/2;
  if (!useDelay)
    delay = 0;
  
  CGRect hideFrame = subview.frame;
  hideFrame.origin.x = self.view.bounds.size.width;
  [UIView animateWithDuration:kRemoveServiceUIDuration delay:delay options:UIViewAnimationCurveEaseInOut animations:^{
    subview.frame = hideFrame;
  } completion:^(BOOL finished) {
    [subview removeFromSuperview];      
  }];
}

- (PSConnectionView*)getViewForConnnectionModel:(PSConnectionModel *)aConnectionModel
{
  for (PSConnectionView *subview in self.scrollView.subviews) {
    if (![subview isKindOfClass:[PSConnectionView class]])
      continue;
    if (![subview.connectionModel isEqual:aConnectionModel])
      continue;
    return subview;
  }
  return nil;
}

- (void)updateScrollViewContentSize
{
  CGRect frame = self.scrollView.bounds;
  
  for (UIView *subview in self.scrollView.subviews)
    if (CGRectGetMaxY(subview.frame) > CGRectGetMaxY(frame))
      frame.size.height = CGRectGetMaxY(subview.frame);
  
  self.scrollView.contentSize = frame.size;
}



#pragma mark - NSNetServiceBrowserDelegate

/**
 * used by service browser - this gets called if a service appears.  Update the tableview here.
 */
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing
{
  [self stopIndicatorSpinning];
  [UIView animateWithDuration:0.5 animations:^{
    self.lblStatus.alpha = 0;
  }];
  
	if ([self hasServiceUI:aNetService])
    return;
  
  [self addServiceUI:aNetService];
}

/**
 * used by service browser - when a service disappears, this guy gets called.  Update the tableview here/
 */
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing
{
	if (![self hasServiceUI:aNetService])
    return;
	
  [self removeServiceUI:aNetService useDelay:NO];
  
  NSMutableArray *serviceList = [[PSSharedData sharedLibrary] getServiceList];
  for (NSNetService *aNetService in serviceList)
    [self updateServiceUI:aNetService];
  
  /*
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, kRemoveServiceUIDuration * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
    NSMutableArray *serviceList = [[PSSharedData sharedLibrary] getServiceList];
    for (NSNetService *aNetService in serviceList)
      [self updateServiceUI:aNetService];
  });
   */
}



#pragma mark - PSConnectionViewDelegate

- (void)psConnectionView:(PSConnectionView*)psConnectionView onButtonClicked:(PSConnectionModel *)connectionModel
{
  self.connectWaitTime = [NSDate date];
  
  NSNumber *connCount = [NSNumber numberWithInt:[self numServiceUI]];
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:connCount, FLURRY_CONNECTION_COUNT, nil];
  [FlurryAnalytics logEvent:FLURRY_CONNECTION_COUNT withParameters:dictionary];
  
  //Tap to cancel
  BOOL hasKeyboard = [[UIKeyboardListener sharedLibrary] isVisible];
  BOOL cancel = self.focusConnectionView != nil && self.focusConnectionView != psConnectionView;
  if (hasKeyboard || cancel) {
    self.focusConnectionView = nil;
    [psConnectionView hidePassword];
    return;
  }
  
  self.focusConnectionView = psConnectionView;
  
  //Hide password field of others
  for (PSConnectionView *subview in self.scrollView.subviews) {
    if (![subview isKindOfClass:[PSConnectionView class]])
      continue;
    if ([subview isEqual:psConnectionView])
      continue;
    [subview hidePassword];
  }
  
  //Retrieve password from keychain, if any
  [[PSSharedData sharedLibrary] retrievePasswordForModel:connectionModel];
  
  //No password, ask for password
  if (connectionModel.serverPassword == nil) {
    [psConnectionView showPassword];
    return;
  }
      
  //Try connecting & wait for delegate callback
  [self startIndicatorSpinning];
  [PSSharedConnection sharedLibrary].delegate = self;
  [[PSSharedConnection sharedLibrary] connect:connectionModel];
  return;
}

- (void)psConnectionView:(PSConnectionView*)psConnectionView onEnterPassword:(PSConnectionModel *)connectionModel
{  
  //No password, ask for password
  if (connectionModel.serverPassword == nil) {
    [psConnectionView showPassword];
    return;
  }
  
  //Try connecting & wait for delegate callback
  [self startIndicatorSpinning];
  [PSSharedConnection sharedLibrary].delegate = self;
  [[PSSharedConnection sharedLibrary] connect:connectionModel];
}



#pragma mark - PSSharedConnectionDelegate

- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection model:(PSConnectionModel*)model onConnection:(BOOL)success
{  
  PSConnectionView *psConnectionView = [self getViewForConnnectionModel:model];
  if (psConnectionView == nil) {
    [self stopIndicatorSpinning];
    return;
  }
  
  //Wrong password
  if (!success) {
    [self stopIndicatorSpinning];
    [psConnectionView showErrorPassword];
    psSharedConnection.lastConnectionModel = nil;
    [FlurryAnalytics logEvent:FLURRY_WRONG_PASSWORD];
    return;
  }
  
  //Successfully connected, no active document
  if (psSharedConnection.lastNumberOfDocuments == 0) {
    [self dismissSelf];
    return;
  }
  
  //Has active document, get the first image
  [psSharedConnection startReceivingImages];
}

- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection receiveImage:(UIImage*)image
{
  [self dismissSelf];
}

- (void)dismissSelf
{
  BOOL hasImage = [PSSharedConnection sharedLibrary].lastNumberOfDocuments > 0;
  NSDate *currentDate = [NSDate date];
  NSTimeInterval timeDifference = fabs([currentDate timeIntervalSinceDate:self.connectWaitTime]);
  NSNumber *waitTime = [NSNumber numberWithInt:round(timeDifference)];
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:waitTime, FLURRY_CONNECTED_WAIT_TIME, nil];
  [FlurryAnalytics logEvent:(hasImage ? FLURRY_CONNECTED_WITH_IMAGE : FLURRY_CONNECTED_NO_IMAGE) withParameters:dictionary];
 
  [self stopIndicatorSpinning];
    
  NSMutableArray *serviceList = [[PSSharedData sharedLibrary] getServiceList];
  for (NSNetService *aNetService in serviceList)
    [self removeServiceUI:aNetService useDelay:YES];
  
  dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.6 * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
    [self dismissModalViewControllerAnimated:NO];
  });
}

@end
