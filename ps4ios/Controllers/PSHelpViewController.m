//
//  PSHelpViewController.m
//  ps4ios
//
//  Created by Torin Nguyen on 27/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "PSHelpViewController.h"

@interface PSHelpViewController ()
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIButton *btnClose;
@property (nonatomic, strong) IBOutlet UILabel *lblVersion;
- (IBAction)onBtnExt:(id)sender;
@end

@implementation PSHelpViewController
@synthesize scrollView;
@synthesize btnClose;
@synthesize lblVersion;


- (void)viewDidLoad
{
  [super viewDidLoad];
  self.view.backgroundColor = [UIColor clearColor];
  
  //Version number
  self.lblVersion.text = [NSString stringWithFormat:@"v%@", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
  
  self.view.alpha = 0;
  [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
    self.view.alpha = 1;
  } completion:^(BOOL finished) {
    
  }];
  
  BOOL isPad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
  
  //Scrolling content size
  int numPages = isPad ? 2 : 4;
  int width = isPad ? [UIScreen mainScreen].bounds.size.height : [UIScreen mainScreen].bounds.size.width;
  CGRect frame = self.scrollView.bounds;
  frame.size.width = numPages * width;
  self.scrollView.contentSize = frame.size;
  
  //Hint of swiping
  //[self.scrollView shakeX];
  float duration = isPad ? 0.5f : 0.3f;
  self.scrollView.contentOffset = CGPointMake(-self.scrollView.bounds.size.width*1.5, 0);
  [UIView animateWithDuration:duration delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
    self.scrollView.contentOffset = CGPointMake(40, 0);
  } completion:^(BOOL finished) {
    
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
      self.scrollView.contentOffset = CGPointZero;
    } completion:^(BOOL finished) {
      
    }];
  }];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
      return UIInterfaceOrientationIsLandscape(interfaceOrientation);
  return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


#pragma mark - Actions

- (IBAction)onBtnExt:(id)sender
{
  [self dismissSelf];
}


#pragma mark - UI helpers

- (void)dismissSelf
{
  [self.btnClose shakeX];
  
  BOOL isPad = UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
  int numPages = isPad ? 2 : 4; 
  
  [UIView animateWithDuration:0.2 delay:0.1 options:UIViewAnimationCurveEaseInOut animations:^{
    self.btnClose.alpha = 0;
    self.scrollView.contentOffset = CGPointMake(self.scrollView.contentOffset.x - 40, 0);
  } completion:^(BOOL finished) {
    
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationCurveEaseInOut animations:^{
      self.scrollView.contentOffset = CGPointMake(self.scrollView.bounds.size.width*numPages, 0);
    } completion:^(BOOL finished) {
      [self dismissModalViewControllerAnimated:NO];
    }];
  }];
}

@end
