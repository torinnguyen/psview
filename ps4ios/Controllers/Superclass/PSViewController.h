//
//  PSViewController.h
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PSViewController : UIViewController

- (BOOL)isKeyBoardVisible;
- (void)onAppBecomeActiveNotification:(NSNotification *)notification;

@end
