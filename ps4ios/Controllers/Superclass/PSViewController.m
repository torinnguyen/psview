//
//  PSViewController.m
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "PSViewController.h"

@interface PSViewController ()

@end

@implementation PSViewController

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  self.view.backgroundColor = [UIColor clearColor];
}

- (void)viewDidUnload
{
  [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  
  //First time app launch will post this notification first, then followed by viewWillAppear
  NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
  [defaultCenter addObserver:self selector:@selector(onAppBecomeActiveNotification:) name:APP_BECOME_ACTIVE_NOTIFICATION object:nil];
  
  NSString *className = [[NSString stringWithFormat:@"%@", [self class]] uppercaseString];
  [FlurryAnalytics logEvent:className timed:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
  NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
  [defaultCenter removeObserver:self];
  
  NSString *className = [[NSString stringWithFormat:@"%@", [self class]] uppercaseString];
  [FlurryAnalytics endTimedEvent:className withParameters:nil];
  
  [super viewWillDisappear:animated];
}

- (void)onAppBecomeActiveNotification:(NSNotification *)notification
{
  NSString *className = [[NSString stringWithFormat:@"%@", [self class]] uppercaseString];
  NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:className, @"class name", nil];
  [FlurryAnalytics logEvent:FLURRY_RETURN_FROM_BACKGROUND withParameters:dictionary];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
  return YES;
}

- (BOOL)isKeyBoardVisible
{
  for (UIWindow *keyboardWindow in [[UIApplication sharedApplication] windows])
    if ([[keyboardWindow description] hasPrefix:@"<UITextEffectsWindow"] == YES)
      return YES;  
  return NO;
}

@end
