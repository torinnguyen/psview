//
//  TNAppDelegate.m
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "TNAppDelegate.h"

#import "PSSplashViewController.h"

@implementation TNAppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  //Keyboard helper
  [UIKeyboardListener sharedLibrary];
  
  //Stats
  [FlurryAnalytics startSession:@"VJXNHRF92RP4BCVXT533"];
  [TestFlight takeOff:@"3e6447aadf6f34b9b13962363708cd1d_MTA0MzIwMjAxMi0wNi0zMCAwMDozOTowNS4yODg1ODM"];
  
  self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
  self.window.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_blueprint"]];

  self.viewController = [[PSSplashViewController alloc] initWithNibName:@"PSSplashViewController" bundle:nil];
  self.window.rootViewController = self.viewController;
  
  [self.window makeKeyAndVisible];
  return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
  //Restart Bonjour everytime
  [[PSSharedData sharedLibrary] saveLastCloseDate];
  [[PSSharedData sharedLibrary] stopListening];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{

}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
  [[PSSharedData sharedLibrary] stopListening];
  [[PSSharedData sharedLibrary] reset];
  [[PSSharedData sharedLibrary] startListening];
  
  [[NSNotificationCenter defaultCenter] postNotificationName:APP_BECOME_ACTIVE_NOTIFICATION object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
