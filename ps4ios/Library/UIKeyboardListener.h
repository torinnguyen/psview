//
//  UIKeyboardListener.h
//  ps4ios
//
//  Created by Torin Nguyen on 27/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIKeyboardListener : NSObject

+ (UIKeyboardListener *)sharedLibrary;
- (BOOL) isVisible;

@end
