//
//  PSSharedData.m
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "PSSharedData.h"

@interface PSSharedData() <NSNetServiceBrowserDelegate, NSNetServiceDelegate>

@property (nonatomic, strong) NSUserDefaults *userDefaults;
@property (nonatomic, strong) NSNetServiceBrowser *serviceBrowser;
@property (nonatomic, strong) NSMutableArray *serviceList;

@end

@implementation PSSharedData
@synthesize bonjourDelegate;
@synthesize userDefaults;
@synthesize lastUsedServerName = _lastUsedServerName;
@synthesize serviceBrowser, serviceList;

+ (PSSharedData *)sharedLibrary
{
  static PSSharedData *_sharedLibrary = nil;
  static dispatch_once_t oncePredicate;
  dispatch_once(&oncePredicate, ^{
    _sharedLibrary = [[self alloc] init];
  });
  
  return _sharedLibrary;
}

- (id)init
{
  self = [super init];
  if (!self)
    return self;

  self.userDefaults = [NSUserDefaults standardUserDefaults];
  self.bonjourDelegate = nil;
  
  NSDictionary *userDefaultsDefaults = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithBool:YES], kSettingJPEG,
                                        [NSNumber numberWithInt:2], kSettingInterval,
                                        [NSNumber numberWithInt:kBluePrintBgTag], kSettingBackground,
                                        nil];
  [self.userDefaults registerDefaults:userDefaultsDefaults];
     
  return self;
}



#pragma mark - Basic data

- (void)retrieveLastUsedServerName
{
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  NSString *value = [prefs stringForKey:kLastUsedServerName];
  if (value == nil || [value length] <= 0)
    self.lastUsedServerName = nil;
}

- (void)setLastUsedServerName:(NSString *)aLastUsedServerName
{
  _lastUsedServerName = aLastUsedServerName;
  
  //Save to NSUserDefaults
  NSString *value = aLastUsedServerName;
  if (aLastUsedServerName == nil)
    value = @"";
  NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
  [prefs setObject:value forKey:kLastUsedServerName];
}

- (NSMutableArray*)getServiceList
{
  return self.serviceList;
}


#pragma mark - Utilities

- (void)reset
{
  if (self.serviceList != nil)
    [self.serviceList removeAllObjects];
  self.serviceList = [[NSMutableArray alloc] init];

  self.serviceBrowser = nil;
  self.lastUsedServerName = nil;
  [self retrieveLastUsedServerName];
}

- (void)startListening
{
  if (self.serviceBrowser == nil) {
    self.serviceBrowser = [[NSNetServiceBrowser alloc] init];
    self.serviceBrowser.delegate = self;
  }
  [self.serviceBrowser searchForServicesOfType:@kNameOfService inDomain:@""];  
}

- (void)stopListening
{
  if (self.serviceBrowser == nil)
    return;
  self.serviceBrowser.delegate = nil;
  [self.serviceBrowser stop];
  self.serviceBrowser = nil;
}



#pragma mark - NSNetServiceBrowserDelegate

/**
 * used by service browser - this gets called if a service appears.  Update the tableview here.
 */
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing
{
  //NSLog(@"Service added: %@", aNetService);
 
	if ([self.serviceList containsObject:aNetService])
    return;
  [self.serviceList addObject:aNetService];
  
  //Not necessary
  //aNetService.delegate = self;
  //[aNetService resolveWithTimeout:30];
  
  if ([self.bonjourDelegate respondsToSelector:@selector(netServiceBrowser:didFindService:moreComing:)])
    [self.bonjourDelegate netServiceBrowser:aNetServiceBrowser didFindService:aNetService moreComing:moreComing]; 
}

/**
 * used by service browser - when a service disappears, this guy gets called.  Update the tableview here/
 */
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing
{
  //NSLog(@"Service removed: %@", aNetService);
  aNetService.delegate = nil;
  
	if (![self.serviceList containsObject:aNetService])
    return;
	[self.serviceList removeObject:aNetService];
  
  if ([self.bonjourDelegate respondsToSelector:@selector(netServiceBrowser:didRemoveService:moreComing:)])
    [self.bonjourDelegate netServiceBrowser:aNetServiceBrowser didRemoveService:aNetService moreComing:moreComing];
}



#pragma mark - NSNetServiceDelegate

- (void)netServiceDidResolveAddress:(NSNetService *)sender
{
  //NSLog(@"Service addresses: %@", sender.addresses);
}




#pragma mark - Keychain

- (BOOL)savePasswordForModel:(PSConnectionModel*)aConnModel
{
  //Which username & password
  NSString *newPassword = aConnModel.serverPassword;
  NSString *username = nil;
  NSNetService *theService = aConnModel.serviceObject;
  BOOL bonjourConnection = aConnModel.serviceObject != nil || aConnModel.serverAddress == nil;
  if (!bonjourConnection)     username = aConnModel.serverAddress;
  else                        username = theService.name;
  
  if (username == nil || newPassword == nil) {
    //NSLog(@"WARNING: username or password is nil in savePasswordForModel");
    return NO;
  }
    
  NSError *error = nil;
  [SFHFKeychainUtils storeUsername:username
                       andPassword:newPassword
                    forServiceName:PS_KEYCHAIN_STRING
                    updateExisting:TRUE 
                             error:&error];
  
  return error == nil;
}

- (BOOL)removePasswordForModel:(PSConnectionModel*)aConnModel
{
  //Which username & password
  NSString *username = nil;
  NSNetService *theService = aConnModel.serviceObject;
  BOOL bonjourConnection = aConnModel.serviceObject != nil || aConnModel.serverAddress == nil;
  if (!bonjourConnection)     username = aConnModel.serverAddress;
  else                        username = theService.name;
  
  if (username == nil) {
    //NSLog(@"WARNING: username or password is nil in savePasswordForModel");
    return NO;
  }

  NSError *error = nil;
  [SFHFKeychainUtils deleteItemForUsername:username
                            andServiceName:PS_KEYCHAIN_STRING 
                                     error:&error];
  return error == nil;
}

- (BOOL)retrievePasswordForModel:(PSConnectionModel*)aConnModel;
{
  //Which username
  NSString *username = nil;
  NSNetService *theService = aConnModel.serviceObject;
  BOOL bonjourConnection = aConnModel.serviceObject != nil || aConnModel.serverAddress == nil;
  if (!bonjourConnection)     username = aConnModel.serverAddress;
  else                        username = theService.name;
  
  if (username == nil)
    return NO;
  
  NSError *error = nil;
  NSString *passwordInKeychain = [SFHFKeychainUtils getPasswordForUsername:theService.name
                                                            andServiceName:PS_KEYCHAIN_STRING
                                                                     error:&error];
  if (passwordInKeychain != nil && [passwordInKeychain length] >= MIN_PASSWORD_LENGTH)
    aConnModel.serverPassword = passwordInKeychain;
  
  if (error)
    aConnModel.serverPassword = nil;
  
  return aConnModel.serverPassword != nil;
}


#pragma mark - NSUserDefault

- (int)getSecondsSinceLastClose
{
  NSDate *lastCloseDate = [self.userDefaults objectForKey:kLastCloseDate];
  if (lastCloseDate == nil)
    return 3600*24*7;
  
  NSDate *currentDate = [NSDate date];
  NSTimeInterval timeDifference = fabs([currentDate timeIntervalSinceDate:lastCloseDate]);
  
  return (int)timeDifference;
}
- (int)getLaunchCount               {  return [self.userDefaults integerForKey:kLaunchCount];         }
- (BOOL)getSettingPin               {  return [self.userDefaults boolForKey:kSettingPin];             }
- (int)getSettingSize               {  return [self.userDefaults integerForKey:kSettingSize];         }
- (int)getSettingBackground         {  return [self.userDefaults integerForKey:kSettingBackground];   }
- (float)getSettingRefreshInterval  {  return [self.userDefaults floatForKey:kSettingInterval];       }
- (BOOL)getSettingJPEG              {  return [self.userDefaults boolForKey:kSettingJPEG];            }
- (NSString*)getSettingAutoConnect  {  return [self.userDefaults stringForKey:kSettingAutoConnect];   }


- (void)increaseLaunchCount
{
  int count = [self getLaunchCount];
  count++;
  [self.userDefaults setInteger:count forKey:kLaunchCount];
  [self.userDefaults synchronize];
}

- (void)saveLastCloseDate                         {    [self.userDefaults setObject:[NSDate date] forKey:kLastCloseDate];   [self.userDefaults synchronize]; }
- (void)setSettingPin:(BOOL)newValue              {    [self.userDefaults setBool:newValue forKey:kSettingPin];             [self.userDefaults synchronize]; }
- (void)setSettingSize:(int)newValue              {    [self.userDefaults setInteger:newValue forKey:kSettingSize];         [self.userDefaults synchronize]; }
- (void)setSettingBackground:(int)newValue        {    [self.userDefaults setInteger:newValue forKey:kSettingBackground];   [self.userDefaults synchronize]; }
- (void)setSettingRefreshInterval:(float)newValue {    [self.userDefaults setFloat:newValue forKey:kSettingInterval];       [self.userDefaults synchronize]; }
- (void)setSettingJPEG:(BOOL)newValue             {    [self.userDefaults setFloat:newValue forKey:kSettingJPEG];           [self.userDefaults synchronize]; }
- (void)setSettingAutoConnect:(NSString*)newValue {    [self.userDefaults setObject:newValue forKey:kSettingAutoConnect];   [self.userDefaults synchronize]; }

@end
