//
//  PSSharedData.h
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kNameOfService          "_photoshopserver._tcp."
#define kLastUsedServerName     @"kLastUsedServerName"
#define kLastCloseDate          @"last_close_date"
#define kLaunchCount            @"launch_count"
#define kSettingPin             @"setting_pinned"
#define kSettingSize            @"setting_size"
#define kSettingBackground      @"setting_background"
#define kSettingInterval        @"setting_refresh_interval"
#define kSettingJPEG            @"setting_jpeg"
#define kSettingAutoConnect     @"setting_auto_connect"
#define kSize11Tag              101
#define kSize12Tag              102
#define kSizeFSTag              103
#define kBluePrintBgTag         8

@class PSSharedData;
@protocol PSSharedDataDelegate <NSObject>
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didFindService:(NSNetService *)aNetService moreComing:(BOOL)moreComing;
- (void)netServiceBrowser:(NSNetServiceBrowser *)aNetServiceBrowser didRemoveService:(NSNetService *)aNetService moreComing:(BOOL)moreComing;
@optional

@end

@interface PSSharedData : NSObject

@property	(nonatomic, unsafe_unretained) id bonjourDelegate;
@property	(nonatomic, strong) NSString *lastUsedServerName;

+ (PSSharedData *)sharedLibrary;
- (void)reset;
- (void)startListening;
- (void)stopListening;

- (NSMutableArray*)getServiceList;

- (BOOL)savePasswordForModel:(PSConnectionModel*)aConnModel;
- (BOOL)removePasswordForModel:(PSConnectionModel*)aConnModel;
- (BOOL)retrievePasswordForModel:(PSConnectionModel*)aConnModel;

- (int)getSecondsSinceLastClose;
- (int)getLaunchCount;
- (BOOL)getSettingPin;
- (int)getSettingSize;
- (int)getSettingBackground;
- (float)getSettingRefreshInterval;
- (BOOL)getSettingJPEG;
- (NSString*)getSettingAutoConnect;

- (void)saveLastCloseDate;
- (void)increaseLaunchCount;
- (void)setSettingPin:(BOOL)newValue;
- (void)setSettingSize:(int)newValue;
- (void)setSettingBackground:(int)newValue;
- (void)setSettingRefreshInterval:(float)newValue;
- (void)setSettingJPEG:(BOOL)newValue;
- (void)setSettingAutoConnect:(NSString*)newValue;

@end
