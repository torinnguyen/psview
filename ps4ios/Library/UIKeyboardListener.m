//
//  UIKeyboardListener.m
//  ps4ios
//
//  Created by Torin Nguyen on 27/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "UIKeyboardListener.h"

@interface UIKeyboardListener()
@property (nonatomic, assign) BOOL visible;
@end

@implementation UIKeyboardListener
@synthesize visible = _visible;

+ (UIKeyboardListener *)sharedLibrary
{
  static UIKeyboardListener *_sharedLibrary = nil;
  static dispatch_once_t oncePredicate;
  dispatch_once(&oncePredicate, ^{
    _sharedLibrary = [[self alloc] init];
  });
  
  return _sharedLibrary;
}

-(id) init
{
  self = [super init];
  if (!self)
    return self;
  
  NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
  [center addObserver:self selector:@selector(noticeShowKeyboard:) name:UIKeyboardDidShowNotification object:nil];
  [center addObserver:self selector:@selector(noticeHideKeyboard:) name:UIKeyboardWillHideNotification object:nil];
  return self;
}

- (void) noticeShowKeyboard:(NSNotification *)inNotification
{
  _visible = true;
}

- (void)noticeHideKeyboard:(NSNotification *)inNotification
{
  _visible = false;
}

- (BOOL)isVisible
{
  return _visible;
}

@end