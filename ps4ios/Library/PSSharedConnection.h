//
//  PSSharedConnection.h
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PSSharedConnection;
@protocol PSSharedConnectionDelegate <NSObject>
@optional
- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection model:(PSConnectionModel*)model onConnection:(BOOL)success;
- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection foregroundColorChanged:(UIColor*)color;
- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection backgroundColorChanged:(UIColor*)color;
- (void)psSharedConnection:(PSSharedConnection*)psSharedConnection receiveImage:(UIImage*)image;
@end

@interface PSSharedConnection : NSObject

@property (nonatomic, unsafe_unretained) id delegate;
@property (nonatomic, strong) PSConnectionModel *lastConnectionModel;
@property (nonatomic, strong) UIImage* lastImage;
@property (nonatomic, strong) UIColor *lastForegroundColor;
@property (nonatomic, strong) UIColor *lastBackgroundColor;
@property (nonatomic, assign) int lastNumberOfDocuments;
@property (nonatomic, assign) BOOL isJPEG;

+ (PSSharedConnection *)sharedLibrary;
+ (PSSharedConnection *)newConnection;

- (void)connect:(PSConnectionModel*)model;
- (void)disconnect;
- (BOOL)isConnected;
- (BOOL)isBadConnection;

- (void)setImageRefreshDuration:(float)second;
- (void)startReceivingImages;
- (void)stopReceivingImages;

- (void)getForegroundColor;
- (void)getBackgroundColor;
- (void)getNumberOfDocuments;
- (void)subscribeColorChanges;

@end
