//
//  PSSharedConnection.m
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "PSSharedConnection.h"
#import "ImageHelper.h"
#import <dispatch/dispatch.h>
#import <objc/message.h>

#define kImageRefreshDuration         2         //default value, can be changed at runtime
#define kServicePort                  49494
#define kMinNumImageRequestHistory    5
#define kMaxNumImageRequestHistory    10

@interface PSSharedConnection() <NSStreamDelegate>
@property (nonatomic, assign) BOOL isConnected;
@property (nonatomic, assign) BOOL isBadConnection;

@property (nonatomic, strong) NSTimer *imageTimer;
@property (nonatomic, strong) NSDate *lastImageDate;
@property (nonatomic, strong) NSDate *lastImageRequestDate;
@property (nonatomic, assign) BOOL imageTimerStopping;
@property (nonatomic, assign) float imageRefreshDuration;
@property (nonatomic, assign) float previous_imageRefreshDuration;
@property (nonatomic, strong) NSMutableArray *imageRequestDurationHistoryPNG;
@property (nonatomic, strong) NSMutableArray *imageRequestDurationHistoryJPEG;

@property (nonatomic, strong) NSInputStream *inputStream;
@property (nonatomic, strong) NSOutputStream *outputStream;
@property (nonatomic, strong) NSMutableData *dataBuffer;
@property (nonatomic, assign) int	packetBodySize;
@end

@implementation PSSharedConnection
{
  dispatch_queue_t backgroundQueue;
  PSCryptorRef sCryptorRef;
  int transaction_id;
  int foregroundColor_subscription;
  int backgroundColor_subscription;
  int foregroundColor_transaction;
  int backgroundColor_transaction;
  int activeDocument_transaction;
}
@synthesize delegate;
@synthesize isConnected = _isConnected;
@synthesize isBadConnection = _isBadConnection;
@synthesize isJPEG = _isJPEG;

@synthesize imageTimer;
@synthesize imageTimerStopping;
@synthesize imageRefreshDuration = _imageRefreshDuration;
@synthesize lastImageDate;
@synthesize lastImageRequestDate;
@synthesize previous_imageRefreshDuration = _previous_imageRefreshDuration;
@synthesize imageRequestDurationHistoryPNG;
@synthesize imageRequestDurationHistoryJPEG;

@synthesize lastImage;
@synthesize lastConnectionModel;
@synthesize lastForegroundColor, lastBackgroundColor, lastNumberOfDocuments;
@synthesize inputStream, outputStream, dataBuffer, packetBodySize;

#pragma mark - Initialization

static PSSharedConnection *_sharedLibrary = nil;

+ (PSSharedConnection *)sharedLibrary
{
  /*
  static dispatch_once_t oncePredicate;
  dispatch_once(&oncePredicate, ^{
    _sharedLibrary = [[self alloc] init];
  });
  */
  
  if (_sharedLibrary != nil)
    return _sharedLibrary;
  
  _sharedLibrary = [[self alloc] init];  
  return _sharedLibrary;
}

+ (PSSharedConnection *)newConnection
{
  _sharedLibrary = nil;
  return [PSSharedConnection sharedLibrary];
}

- (id)init
{
  self = [super init];
  if (!self)
    return self;
    
  backgroundQueue = nil;
  transaction_id = 1;
  foregroundColor_subscription = -1;
  backgroundColor_subscription = -1;
  foregroundColor_transaction = -1;
  backgroundColor_transaction = -1;
  activeDocument_transaction = -1;
  sCryptorRef = nil;
  
  self.inputStream = nil;
  self.outputStream = nil;
  self.packetBodySize = -1;
  self.dataBuffer = nil;
  self.isConnected = NO;

  if (backgroundQueue == nil)
    backgroundQueue = dispatch_queue_create("com.torinnguyen.ps4ios.backgroundQueue", NULL);

  self.delegate = nil;
  self.isJPEG = YES;
  self.imageRefreshDuration = kImageRefreshDuration;
  self.previous_imageRefreshDuration = self.imageRefreshDuration;
  
  return self;
}

- (void)reset
{
  self.isBadConnection = NO;
  self.lastImageDate = NO;
  self.lastImageRequestDate = NO;
  self.lastImage = nil;
  self.isConnected = NO;
  self.packetBodySize = -1;
  self.dataBuffer = nil; 
  transaction_id = 1;
  foregroundColor_subscription = -1;
  backgroundColor_subscription = -1;
  foregroundColor_transaction = -1;
  backgroundColor_transaction = -1;
  
  if (self.imageRequestDurationHistoryPNG != nil)
    [self.imageRequestDurationHistoryPNG removeAllObjects];
  self.imageRequestDurationHistoryPNG = [[NSMutableArray alloc] init];

  if (self.imageRequestDurationHistoryJPEG != nil)
    [self.imageRequestDurationHistoryJPEG removeAllObjects];
  self.imageRequestDurationHistoryJPEG = [[NSMutableArray alloc] init];
}

- (void)logMessage:(NSString *) msg
{
  //NSLog(@"%@", msg);
}



#pragma mark - Public interface

- (void)setImageRefreshDuration:(float)second
{
  self.previous_imageRefreshDuration = self.imageRefreshDuration;
  _imageRefreshDuration = second;
}

- (float)getAverageImageRequestDuration:(BOOL)jpeg
{
  int count = jpeg ? [self.imageRequestDurationHistoryJPEG count] : [self.imageRequestDurationHistoryPNG count];
  if (count < kMinNumImageRequestHistory)
    return 0;
  
  float sum = 0;
  if (jpeg) {
    for (NSNumber *number in self.imageRequestDurationHistoryJPEG)
      if ([number isKindOfClass:[NSNumber class]])
        sum += number.floatValue;
  }
  else {
    for (NSNumber *number in self.imageRequestDurationHistoryPNG)
      if ([number isKindOfClass:[NSNumber class]])
        sum += number.floatValue;
  }
  return sum / count;
}
       
- (void)startReceivingImages
{
  self.imageTimerStopping = NO;
  if (self.imageTimer != nil || [self.imageTimer isValid])
    return;
  self.imageTimer = [NSTimer scheduledTimerWithTimeInterval:self.imageRefreshDuration
                                                     target:self
                                                   selector:@selector(onImageTimer:)
                                                   userInfo:nil
                                                    repeats:YES];
}

- (void)stopReceivingImages
{
  self.imageTimerStopping = YES;
  if (self.imageTimer == nil || ![self.imageTimer isValid])
    return;
  [self.imageTimer invalidate];
  self.imageTimer = nil; 
}



#pragma mark - Background thead

- (void)performAsyncSelector:(SEL)aSelector
{
  if (![self respondsToSelector:aSelector])
    return;
      
  dispatch_async(backgroundQueue, ^(void) {
    //[self performSelector:aSelector];
    objc_msgSend(self, aSelector);
  }); 
}

- (void)onImageTimer:(NSTimer *)timer
{
  //Stop the timer
  if (self.imageTimerStopping || !self.isConnected) {
    [self.imageTimer invalidate];
    self.imageTimer = nil; 
    return;
  }
  
  //When a new image is received, this flag will be cleared
  //Otherwise, it's an indicator that the last request has not completed, we should not request new one
  if (self.lastImageRequestDate != nil) {
    NSDate *currentDate = [NSDate date];
    NSTimeInterval timeDifference = fabs([currentDate timeIntervalSinceDate:self.lastImageDate]);
    if (timeDifference < MAX(10, self.imageRefreshDuration+0.5))
      return;
  }
  
  //Change refresh duration if needed
  if (self.previous_imageRefreshDuration != self.imageRefreshDuration) {
    [self.imageTimer invalidate];
    self.imageTimer = nil; 
    self.imageTimer = [NSTimer scheduledTimerWithTimeInterval:self.imageRefreshDuration
                                                       target:self
                                                     selector:@selector(onImageTimer:)
                                                     userInfo:nil
                                                      repeats:YES];
  }
  self.previous_imageRefreshDuration = self.imageRefreshDuration;
  
  //New async request
  [self performAsyncSelector:@selector(requestImageFromPhotoshop)];
}



#pragma mark - High-level connection helpers

- (BOOL)isConnected
{
  return _isConnected;
}

- (BOOL)isBadConnection
{
  return _isBadConnection;
}

- (void)disconnect
{
  [self stopReceivingImages];
  [self closeStreams];
  self.delegate = nil;
}

- (void)connect:(PSConnectionModel*)model
{  
  //Validate the model
  if (model == nil)
    return;
  if (model.serverPassword == nil || [model.serverPassword length] < MIN_PASSWORD_LENGTH ||
      (model.serviceObject == nil && model.serverAddress == nil))
    return;
 
  //If still connected to something, close it
  [self stopReceivingImages];
  if (self.inputStream || self.outputStream)
    [self closeStreams];
  
  //New cryptor object based on password
  if (sCryptorRef)
    DestroyPSCryptor(sCryptorRef);
  sCryptorRef = CreatePSCryptor([model.serverPassword UTF8String]);

  //Reset cache
  [self reset];
  self.lastConnectionModel = model;
  
  //Use custome IP Address or Host name
  BOOL bonjourConnection = model.serviceObject != nil || model.serverAddress == nil;
  if (!bonjourConnection)
  {
    // use CFStreamCratePairWithSocketToHost();
    // then cast it to NSInputSteam and NSOutputStream
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    
    CFStringRef cfstringServerAddress = (__bridge CFStringRef)model.serverAddress;   
    CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, cfstringServerAddress, kServicePort, &readStream, &writeStream);
    
    self.inputStream = objc_unretainedObject(readStream);
    self.outputStream = objc_unretainedObject(writeStream);
    
    [self openStreams:@"Photoshop"];
    [self getForegroundColor];
    [self getBackgroundColor];
    [self getNumberOfDocuments];
    return;
  }
  
  //Bonjour connection
  NSInputStream *tempInputStream = nil;
	NSOutputStream *tempOutputStream = nil;
  NSNetService *theService = model.serviceObject;
  if ([theService getInputStream:&tempInputStream outputStream:&tempOutputStream])
  {
    self.inputStream = tempInputStream;
    self.outputStream = tempOutputStream;
    
    [self openStreams:theService.name];
    [self getForegroundColor];
    [self getBackgroundColor];
    [self getNumberOfDocuments];
    return;
  }
}

- (void)onConnectionSuccess
{
  [[PSSharedData sharedLibrary] savePasswordForModel:self.lastConnectionModel];
  
  NSString *serverName = @"";
  BOOL bonjourConnection = self.lastConnectionModel.serviceObject != nil || self.lastConnectionModel.serverAddress == nil;
  if (bonjourConnection) {
    NSNetService *theService = self.lastConnectionModel.serviceObject;
    serverName = theService.name;
  }
  [self logMessage:[NSString stringWithFormat:@"Connected to %@", serverName]];
  
  if ([self.delegate respondsToSelector:@selector(psSharedConnection:model:onConnection:)])
    [self.delegate psSharedConnection:self model:self.lastConnectionModel onConnection:YES];
  
  [self subscribeColorChanges];
}

- (void)onConnectionDisconnect
{
  if (self.isBadConnection)
    [[PSSharedData sharedLibrary] removePasswordForModel:self.lastConnectionModel];
  
  NSString *serverName = @"";
  BOOL bonjourConnection = self.lastConnectionModel.serviceObject != nil || self.lastConnectionModel.serverAddress == nil;
  if (bonjourConnection) {
    NSNetService *theService = self.lastConnectionModel.serviceObject;
    serverName = theService.name;
  }
  [self logMessage:[NSString stringWithFormat:@"Disconnected from %@", serverName]];
  
  if ([self.delegate respondsToSelector:@selector(psSharedConnection:model:onConnection:)])
    [self.delegate psSharedConnection:self model:self.lastConnectionModel onConnection:NO];
}



#pragma mark - Photoshop API wrapper

- (void)getForegroundColor
{
  NSString * stringToSend = [NSString stringWithUTF8String:"\"ignored\\r\"+app.foregroundColor.rgb.hexValue.toString();"];
  NSData * dataToSend = [stringToSend dataUsingEncoding:NSUTF8StringEncoding];
  [ self sendJavaScriptMessage: dataToSend ];
  foregroundColor_transaction = transaction_id - 1;
}

- (void)getBackgroundColor
{
  NSString * stringToSend = [NSString stringWithUTF8String:"\"ignored\\r\"+app.backgroundColor.rgb.hexValue.toString();"];
  NSData * dataToSend = [stringToSend dataUsingEncoding:NSUTF8StringEncoding];
  [ self sendJavaScriptMessage: dataToSend ];
  backgroundColor_transaction = transaction_id - 1;
}

- (void)getNumberOfDocuments
{
  NSString * stringToSend = [NSString stringWithUTF8String:"\"ignored\\r\"+app.documents.length;"];
  NSData * dataToSend = [stringToSend dataUsingEncoding:NSUTF8StringEncoding];
  [ self sendJavaScriptMessage: dataToSend ];
  activeDocument_transaction = transaction_id - 1;
}

- (void)subscribeColorChanges
{
	// subscribe to foreground changes
  NSString * stringToSend = [NSString stringWithUTF8String:"var idNS = stringIDToTypeID( \"networkEventSubscribe\" );"
                             "var desc1 = new ActionDescriptor();"
                             "desc1.putClass( stringIDToTypeID( \"eventIDAttr\" ), stringIDToTypeID( \"foregroundColorChanged\" ) );"
                             "executeAction( idNS, desc1, DialogModes.NO );"  ];
  NSData * dataToSend = [stringToSend dataUsingEncoding:NSUTF8StringEncoding];
  [ self sendJavaScriptMessage: dataToSend ];
  foregroundColor_subscription = transaction_id - 1;
	
	// subscribe to background changes
  NSString * stringToSend2 = [NSString stringWithUTF8String:"var idNS = stringIDToTypeID( \"networkEventSubscribe\" );"
                              "var desc1 = new ActionDescriptor();"
                              "desc1.putClass( stringIDToTypeID( \"eventIDAttr\" ), stringIDToTypeID( \"backgroundColorChanged\" ) );"
                              "executeAction( idNS, desc1, DialogModes.NO );"  ];
  NSData * dataToSend2 = [stringToSend2 dataUsingEncoding:NSUTF8StringEncoding];
  [ self sendJavaScriptMessage: dataToSend2 ];
  backgroundColor_subscription = transaction_id - 1;
}

- (void)requestImageFromPhotoshop
{
	if (!self.isConnected)
    return;

  NSString *stringToSend;
  
  if (self.isJPEG)
  {
    stringToSend = [NSString stringWithUTF8String:"var idNS = stringIDToTypeID( \"sendDocumentThumbnailToNetworkClient\" );\r"
                    "var desc1 = new ActionDescriptor();\r"
                    "desc1.putInteger( stringIDToTypeID( \"width\" ), 2048 );\r"
                    "desc1.putInteger( stringIDToTypeID( \"height\" ), 2048 );\r"
                    "desc1.putInteger( stringIDToTypeID( \"format\" ), 1 );\r"
                    "executeAction( idNS, desc1, DialogModes.NO );"  ];
  }
  
  //Pixmap
  else 
  {
    stringToSend = [NSString stringWithUTF8String:"var idNS = stringIDToTypeID( \"sendDocumentThumbnailToNetworkClient\" );"
                    "var desc1 = new ActionDescriptor();"
                    "desc1.putInteger( stringIDToTypeID( \"width\" ), 2048 );"
                    "desc1.putInteger( stringIDToTypeID( \"height\" ), 2048 );"
                    "desc1.putInteger( stringIDToTypeID( \"format\" ), 2 );"
                    "executeAction( idNS, desc1, DialogModes.NO );"  ];
  }
  
  self.lastImageRequestDate = [NSDate date];
  NSData * dataToSend = [stringToSend dataUsingEncoding:NSUTF8StringEncoding];
  [self sendJavaScriptMessage: dataToSend];
}

/**
 *  Take whatever is in ImageView and send it to Photoshop
 *  Blocking function call ?
 */
-(IBAction)sendImageToPhotoshop:(UIImage*)image
{
  if (!image || !self.isConnected)
    return;
	
  if (self.isJPEG)
  {
    [self logMessage: [NSString stringWithFormat:@"INFO: Sending image of width = %1.2f, height = %1.2f to PS...", 
                       image.size.width, image.size.height]]; 
    
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    // need one byte before the image data for format type
    NSMutableData *image_message = [NSMutableData alloc];
    
    unsigned char format = 1; // JPEG
    [image_message appendBytes:(const void *)&format length:1];
    [image_message appendData:imageData];
    
    [self sendNetworkMessage:image_message type:3 ];  // type 3 = JPEG
  }
  
  //Pixmap
  [self logMessage: [NSString stringWithFormat:@"INFO: Sending pixmap of width = %1.2f, height = %1.2f to PS...", 
                     image.size.width, image.size.height]]; 
  
  // first, translate UIImage to rawRGBA8
  unsigned char *buffer = [ImageHelper convertUIImageToBitmapRGBA8:image];
  int width = image.size.width;
  int height = image.size.height;
  int planes = 3;
  
  // next, remove the extra plane;  from 4 to 3.  Note that Photoshop accepts both
  unsigned char *newBuffer = [ImageHelper convertBitmapRGBA8ToPixmapRGB:buffer withWidth:width withHeight:height];			
  NSMutableData *imageData = [[NSMutableData alloc] initWithBytes:(const void *)newBuffer length:(width * height * planes)];
  
  // construct the header
  NSMutableData *image_message = [NSMutableData alloc];
  
  unsigned char format = 2;		// Pixmap
  [ image_message appendBytes:(const void *)&format length:1 ];
  
  // 4 bytes uint32 width
  unsigned int temp = htonl( width );
  [ image_message appendBytes:(const void *)&temp length:4 ];
  
  // 4 bytes uint32 height
  temp = htonl( height );
  [ image_message appendBytes:(const void *)&temp length:4 ];
  
  // 4 bytes uint32 rowBytes
  temp = htonl( planes*width );
  [ image_message appendBytes:(const void *)&temp length:4 ];
  
  // 1 byte color mode = 1 = RGB
  unsigned char tempC = 1;
  [ image_message appendBytes:(const void *)&tempC length:1 ];
  
  // 1 byte channel count
  tempC = planes;
  [ image_message appendBytes:(const void *)&tempC length:1 ];
  
  // 1 byte bits per channel
  tempC = 8;
  [ image_message appendBytes:(const void *)&tempC length:1 ];
  
  // append the bits
  [ image_message appendData: imageData ];
  
  // send the message
  [ self sendNetworkMessage:image_message type:3 ];
  
  if (buffer)
    free(buffer);
  if (newBuffer)
    free(newBuffer);
}



#pragma mark - High-level connection events

- (void)processForegroundColor:(UIColor *)theColor 
{
  self.lastForegroundColor = theColor;
  
  [self logMessage:[NSString stringWithFormat:@"Foreground color: %@", theColor]];
  if ([self.delegate respondsToSelector:@selector(psSharedConnection:foregroundColorChanged:)])
    [self.delegate psSharedConnection:self foregroundColorChanged:theColor];
}

- (void)processBackgroundColor:(UIColor *)theColor 
{
  self.lastBackgroundColor = theColor;
    
  [self logMessage:[NSString stringWithFormat:@"Background color: %@", theColor]];
  if ([self.delegate respondsToSelector:@selector(psSharedConnection:backgroundColorChanged:)])
    [self.delegate psSharedConnection:self backgroundColorChanged:theColor];
}

- (void)processNumberOfDocuments:(int)numDocuments
{
  self.lastNumberOfDocuments = numDocuments;
  
  //First time after connected
  if (!self.isConnected) {
    self.isConnected = YES;
    [self onConnectionSuccess];
  }
}

- (void)processReceivedImage:(UIImage *)image isJPEG:(BOOL)jpeg
{
  self.lastImage = image;
  self.lastImageDate = [NSDate date];
        
  //Stop the timer
  if (self.imageTimerStopping) {
    [self.imageTimer invalidate];
    self.imageTimer = nil; 
    return;
  }
    
  //Measure request time
  NSDate *currentDate = [NSDate date];
  NSTimeInterval timeDifference = fabs([currentDate timeIntervalSinceDate:self.lastImageRequestDate]);
  self.lastImageRequestDate = nil;
  
  NSMutableArray *timeArray = jpeg ? self.imageRequestDurationHistoryJPEG : self.imageRequestDurationHistoryPNG;
  if ([timeArray count] > kMaxNumImageRequestHistory)
    [timeArray removeObjectAtIndex:0];
  [timeArray addObject:[NSNumber numberWithFloat:timeDifference]];
  
  //Adjust the request duration accordingly
  float averageDuration = [self getAverageImageRequestDuration:jpeg];
  if (averageDuration > 0) {
    float newDuration = averageDuration + 0.5;
    if (newDuration < 1.5)
      newDuration = 1.5;
    [self setImageRefreshDuration:newDuration];
    [self logAverageDuration];
  } else {
    [self setImageRefreshDuration:2];
  }
  
  //NSLog(@"Received %@ image: %1.1f x %1.1f", jpeg ? @"JPEG" : @"PNG", image.size.width, image.size.height);
  
  if ([self.delegate respondsToSelector:@selector(psSharedConnection:receiveImage:)])
    [self.delegate psSharedConnection:self receiveImage:image];
  
  //Continous firing
  if (self.imageRefreshDuration <= 0) {
    [self.imageTimer invalidate];
    self.imageTimer = nil;
    [self performAsyncSelector:@selector(requestImageFromPhotoshop)];
  }
}

- (void)logAverageDuration
{
  float averageDurationJpg = [self getAverageImageRequestDuration:YES];
  float averageDurationPng = [self getAverageImageRequestDuration:NO];
  
  if (averageDurationJpg > 0) {
    NSNumber *duration = [NSNumber numberWithInt:round(averageDurationJpg)];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:duration, FLURRY_AVERAGE_DURATION_JPEG, nil];
    [FlurryAnalytics logEvent:FLURRY_AVERAGE_DURATION_JPEG withParameters:dictionary];    
  }
  
  if (averageDurationPng > 0) {
    NSNumber *duration = [NSNumber numberWithInt:round(averageDurationPng)];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:duration, FLURRY_AVERAGE_DURATION_PNG, nil];
    [FlurryAnalytics logEvent:FLURRY_AVERAGE_DURATION_PNG withParameters:dictionary];
  }
}

#pragma mark - Low-level connection helpers

/**
 * Opens both self.inputStream and outputSteam, not authenticated yet
 */
- (void)openStreams: (NSString *)nameOfService
{
  [self.inputStream setDelegate:self];
  [self.outputStream setDelegate:self];
  [self.inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  [self.outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  [self.inputStream open];
  [self.outputStream open];
	self.packetBodySize = -1;
	self.isConnected = NO;
}

/**
 * Closes both self.inputStream and outputSteam, everything will be forced stopped
 */
- (void)closeStreams
{
  [self.inputStream close];
  [self.outputStream close];
  [self.inputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  [self.outputStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  [self.inputStream setDelegate:nil];
  [self.outputStream setDelegate:nil];
  self.inputStream = nil;
  self.outputStream = nil;
	self.packetBodySize = -1;
	if (sCryptorRef)
		DestroyPSCryptor(sCryptorRef);
	sCryptorRef = nil;
	self.isConnected = NO;
  
  [self onConnectionDisconnect];
}

/**
 * The code that send javascript to stream
 * @param dataToSend a UTF8 encoded string
 */
- (void)sendJavaScriptMessage:(NSData*)dataToSend 
{
	[self sendNetworkMessage:dataToSend type:2];
}

/**
 * The code that send javascript to stream
 * @param dataToSend a UTF8 encoded string
 */
- (void)sendNetworkMessage:(NSData*)dataToSend type:(int)dataType
{
  if (self.outputStream) 
	{
    int remainingToWrite = 0;
		
		if (dataToSend != nil)
			remainingToWrite = [dataToSend length];
		
		/* -------------------------------------------------------
     we're writing things in this order:
     Unencrypted:
     1. total message length (not including the length itself)
     2. communication stat
     Encrypted:
     3. Protocol version
     4. Transaction ID
     5. Message type
     6. The message itself		 
     ------------------------------------------------------- */ 
		int prolog_length = 12; // 3x 32 bit integers, not counting the length value itself
		
		int plainTextLength = prolog_length + remainingToWrite;
		size_t encryptedLength = plainTextLength;
		
		encryptedLength = CryptorGetEncryptedLength(plainTextLength);
    
		// ---- UNENCRYPTED PART ------
		// write length of message as 32 bit signed int, includes all bytes after the length
		int swabbed_temp = htonl( encryptedLength + 4 );
		NSInteger bytesWritten = [self.outputStream write:(const uint8_t*)&swabbed_temp maxLength:4];	
		
		// check to see if we're able to write to stream
		if (bytesWritten != 4)
		{
			NSError *err = [self.outputStream streamError];
			[self logMessage:[NSString stringWithFormat:@"ERROR: Did not successful write to stream because: %@", [err localizedFailureReason]]];
			if ([[err localizedFailureReason] localizedCaseInsensitiveCompare:@"Operation timed out"] == NSOrderedSame)
				[self closeStreams];
			return;
		}
		
		/* 
		 stream status has the following status:  (check Apple's Developer Reference for more info)
		 NSStreamStatusNotOpen = 0,
		 NSStreamStatusOpening = 1,
		 NSStreamStatusOpen = 2,
		 NSStreamStatusReading = 3,
		 NSStreamStatusWriting = 4,
		 NSStreamStatusAtEnd = 5,
		 NSStreamStatusClosed = 6,
		 NSStreamStatusError = 7		 
		 */
		NSStreamStatus status = [self.outputStream streamStatus];
		if ( status == NSStreamStatusError ||
        status == NSStreamStatusClosed ||
        status == NSStreamStatusNotOpen )
		{
			NSError *err = [self.outputStream streamError];
			[self logMessage:[NSString stringWithFormat:@"ERROR: %@", [err localizedFailureReason]]];
			if ([[err localizedFailureReason] localizedCaseInsensitiveCompare:@"Operation timed out"] == NSOrderedSame)
				[self closeStreams];
			return;
		}
    
		// the communication status is NOT encrypted
		// write communication status value as 32 bit unsigned int
		swabbed_temp = htonl( 0 );
		[self.outputStream write:(const uint8_t*)&swabbed_temp maxLength:4];
		
		// ------------------------------------------------
		// Encrypted section, until the end of the message		
		char *tempBuffer = (char *) malloc (encryptedLength);
		
		// protocol version, 32 bit unsigned integer
		swabbed_temp = htonl( 1 );
		memcpy (tempBuffer+0, (const void *) &swabbed_temp, 4);
		
		// transaction id, 32 bit unsigned integer
		swabbed_temp = htonl( transaction_id++ );
		memcpy (tempBuffer+4,(const void *) &swabbed_temp, 4);
		
		// content type, 32 bit unsigned integer
		swabbed_temp = htonl( dataType );		// javascript = 2
		memcpy (tempBuffer+8, (const void *) &swabbed_temp, 4);
		
		// the data to transmit
		if (dataToSend != nil)
		{
			uint8_t * dataBuf = (uint8_t *)[dataToSend bytes];
			memcpy (tempBuffer+12, dataBuf, remainingToWrite);
		}
    
		uint8_t * marker = (uint8_t *) tempBuffer;
    
		// now encrypt the message packet
		if (sCryptorRef)
			EncryptDecrypt (sCryptorRef, true, tempBuffer, plainTextLength, tempBuffer, encryptedLength, &encryptedLength);
		else
			[self logMessage:@"PROBLEM: Unable to Encrypt the message because cryptor ref is NULL!"];
    
    // write the text
    remainingToWrite = encryptedLength;
    while (0 < remainingToWrite) {
      int actuallyWritten = 0;
      actuallyWritten = [self.outputStream write:marker maxLength:remainingToWrite];
			
			// make sure we wrote to stream successfully
			status = [self.outputStream streamStatus];
			if (actuallyWritten == 0 ||
          status == NSStreamStatusError ||
          status == NSStreamStatusClosed ||
          status == NSStreamStatusNotOpen)
			{
				NSError *err = [self.outputStream streamError];
				[self logMessage:[NSString stringWithFormat:@"ERROR: %@", [err localizedFailureReason]]];
				free (tempBuffer);
				return;
			}
			
      remainingToWrite -= actuallyWritten;
      marker += actuallyWritten;
    }
		free (tempBuffer);
		// encrypt until the end of message
  }
	
}


#pragma mark - Low-level connection event

/**
 * Handle steam events. Part of NSStreamDelegate Protocol
 * @param aStream incoming stream
 * @param streamEvent the stream event
 */
- (void)stream:(NSStream *)aStream handleEvent:(NSStreamEvent)streamEvent {
  NSInputStream * istream;
  switch(streamEvent) 
	{
    case NSStreamEventEndEncountered:;
      [self closeStreams];
			[self logMessage:@"End encountered, closing stream"];
      break;
    case NSStreamEventHasSpaceAvailable:
      //[self logMessage:@"NSStreamEventHasSpaceAvailable"];
      break;
    case NSStreamEventErrorOccurred:
      [self logMessage:@"NSStreamEventErrorOccurred"];
      break;
    case NSStreamEventOpenCompleted:
      //[self logMessage:@"NSStreamEventOpenCompleted"];
      break;
    case NSStreamEventNone:
      //[self logMessage:@"NSStreamEventNone"];
      break;
    default:
      break;
      
    case NSStreamEventHasBytesAvailable:;
			      
			UInt8 buffer[1024];
			unsigned int actuallyRead = 0;
			
      istream = (NSInputStream *)aStream;
      if (self.dataBuffer == nil)
        self.dataBuffer = [[NSMutableData alloc] initWithCapacity:2048];
			
			actuallyRead = [istream read:buffer maxLength:1024];
			
			[self.dataBuffer appendBytes:buffer length:actuallyRead];
			
			// see if we have enough to process, loop over messages in buffer
			while( YES ) 
			{
				
				// Did we read the header yet?
				if ( packetBodySize == -1 ) 
				{
					// Do we have enough bytes in the buffer to read the header?
					if ( [self.dataBuffer length] >= sizeof(int) ) {
						// extract length
						memcpy(&packetBodySize, [self.dataBuffer bytes], sizeof(int));
						packetBodySize = ntohl( packetBodySize );		// size is in network byte order
						
						// remove that chunk from buffer
						NSRange rangeToDelete = {0, sizeof(int)};
						[self.dataBuffer replaceBytesInRange:rangeToDelete withBytes:NULL length:0];
					}
					else {
						// We don't have enough yet. Will wait for more data.
						break;
					}
				}
        
        //----------------------------------------------------------------------
				
				// We should now have the header. Time to extract the body.
				if ( [self.dataBuffer length] >= ((NSUInteger) packetBodySize) ) 
				{
					// We now have enough data to extract a meaningful packet.
					const int kPrologLength = 16;
					char *buffer = (char *)[self.dataBuffer bytes];
										
					// fetch the communication status
					unsigned long com_status = *((unsigned long *)(buffer + 0));
					com_status = ntohl( com_status );
          
					// decrypt the message
					size_t decryptedLength = (size_t) packetBodySize - 4;  // don't include com status
					
					int skip_message = 0;
					
					if (com_status == 0 && sCryptorRef)
					{
						PSCryptorStatus decryptResult = EncryptDecrypt (sCryptorRef, false, buffer+4, decryptedLength, buffer+4, decryptedLength, &decryptedLength);
						
            //This happens when password in Photoshop is changed half-way
						if (kCryptorSuccess != decryptResult)
						{
							// failed to decrypt.  Ingore messageg and disconnect
							[self logMessage:@"ERROR: Decryption failed. Wrong password."];
              skip_message = 1;
              self.isConnected = NO;
              self.isBadConnection = YES;
						}
					}
					else 
					{
						if (com_status != 0) {
							[self logMessage:@"ERROR: Problem with communication, possible wrong password."];
              self.isConnected = NO;
              self.isBadConnection = YES;
            }
            
            if (!sCryptorRef) {
              [self logMessage:@"ERROR: sCryptorRef is NULL, possible reason being that password was not supplied or password binding function failed."];
              self.isConnected = NO;
              self.isBadConnection = YES;
            }
          }
          
					// Interpret encrypted section
					if (!skip_message)
					{
						// version, 32 bit unsigned int, network byte order
						unsigned long protocol_version = *((unsigned long *)(buffer + 4));
						protocol_version = ntohl( protocol_version );
						
						if (protocol_version != 1)
						{
							// either the message is corrupted or the protocol is newer.
							[self logMessage:@"Incoming protocol version is different the expected. (or the message is corrupted.)  Not processing.\n"];
							skip_message = 1;
						}
            
						if (!skip_message)
						{
							// transaction, 32 bit unsigned int, network byte order
							unsigned long transaction = *((unsigned long *)(buffer + 8));
							transaction = ntohl( transaction );
              
							// content type, 32 bit unsigned int, network byte order
							unsigned long content = *((unsigned long *)(buffer + 12));
							content = ntohl( content );
              
							unsigned char *received_data = (unsigned char *)(buffer+kPrologLength);
							int received_length = (decryptedLength-(kPrologLength-4));
							
							if (content == 3) // image data
							{
								// process image data
								unsigned char image_type = *((unsigned char *)received_data);								
								
								if (image_type == 1) // JPEG
								{
									int jpeg_length = received_length - 1;
									NSData *imageData = [NSData dataWithBytes:(received_data+1) length:jpeg_length];

                  // note that this would be NULL if the data isn't readable
                  UIImage *image = [[UIImage alloc] initWithData:imageData];
                  if (image)
                    [self processReceivedImage:image isJPEG:YES];
								}
								else if (image_type == 2) // Pixmap
								{
									BOOL imageError = NO;
									
									int pixmap_prolog_length = 3*4 + 3 + 1;
									
									int width = *((unsigned long *)(received_data + 1));
									width = ntohl(width);
									
									int height = *((unsigned long *)(received_data + 5));
									height = ntohl( height );
									
									int rowBytes = *((unsigned long *)(received_data + 9));
									rowBytes = ntohl( rowBytes );
									
									int mode = *((unsigned char *)received_data + 13);	// must be 1 for now
									int channels = *((unsigned char *)received_data + 14);
									int bitsPerChannel = *((unsigned char *)received_data + 15);	// must be 8 for now
									
									// this is the original raw data from Photoshop.  It's in RGB format
									const unsigned char *temp_raw_data = received_data + pixmap_prolog_length;									
									int raw_length = received_length - pixmap_prolog_length;
									
									// need to convert it to an RGBA8 because iOS requires it.  If you try to use
									// plain RGB pixmap, the bind will probably return a NULL object
									unsigned char *raw_data = [ImageHelper padPixmapRGBToBitmapRGBA8:(unsigned char *)temp_raw_data withWidth:width withHeight:height];
									
									if (mode != 1 || bitsPerChannel != 8)
										imageError = YES;
									
									// 4k being a largish display size
									if (width < 0 || width > 4096
                      || height < 0 || height > 4096)
										imageError = YES;
									
									if (channels < 0 || channels > 4
                      || rowBytes < ((width*channels*bitsPerChannel)/8))
										imageError = YES;
									
									if (raw_length < (height*rowBytes))
										imageError = YES;
									
									
									if (!imageError)
									{
										/* *** EXTRA NOTE! ****  pixmap on iOS is very unstable - at least for me
										 * My first problem with it is that if it's not RGBA, iOS flat out rejects it with a NULL ptr
										 * Once the RGB raw pixmap is padded, it will display only ONCE.  At this point, the UIImage ptr
										 * inside UIImageView is whacked.  If you assign anything to it, it will blow up.
										 * unless you have a serious need for pixmap, try using JPEG
										 */
										UIImage *imageToDisplay = [ImageHelper convertBitmapRGBA8ToUIImage:raw_data withWidth:width withHeight:height];
										[self processReceivedImage:imageToDisplay isJPEG:NO];									
										free(raw_data);
									}
									else
									{
										[self logMessage:@"Bad parameters for Pixmap!"];
									}
								}
								else 
								{
									[self logMessage:@"Unknown image type."];									
								}
                
							}
							else 
							{
								// Set the response string
								NSString * string = [[NSString alloc] initWithBytes:received_data length:received_length encoding:NSUTF8StringEncoding];
								
								// see if this is a response we're looking for
								if (content != 1)
								{
									if (transaction == foregroundColor_subscription || transaction == foregroundColor_transaction) {
                    UIColor *theColor = [self colorFromResponseString:string];
                    if (theColor != nil)
                      [self processForegroundColor:theColor];
                  }

									if (transaction == backgroundColor_subscription || transaction == backgroundColor_transaction) {
                    UIColor *theColor = [self colorFromResponseString:string];
                    if (theColor != nil)
                      [self processBackgroundColor:theColor];
                  }
                  
                  if (transaction == activeDocument_transaction) {
                    NSRange range = [string rangeOfString:@"\r" ];
                    if (range.length != 0) {                 
                      NSString *num = [string substringFromIndex:(range.location + 1)];
                      [self processNumberOfDocuments:[num intValue]];
                    }
                  }
                  
								}

							}
						}
            
					}
					
					// Remove that chunk from buffer
					NSRange rangeToDelete = {0, packetBodySize};
					[self.dataBuffer replaceBytesInRange:rangeToDelete withBytes:NULL length:0];
					
					// We have processed the packet. Resetting the state.
					packetBodySize = -1;
				}
				else {
					// Not enough data yet. Will wait.
					break;
				}
			}
			
      break;
  }
  
  //No point keeping the connection
  if (self.isBadConnection)
    [self disconnect];
}



#pragma mark - Low-level UI related stuff

/**
 *  takes a hex color and translate it into a UIColor object
 *  @param hexColor the hex color
 *  @return the translated color object
 */
- (UIColor*)colorFromString:(NSString*)hexColor
{	
	if ([hexColor length] < 6)
		return nil;
	
	long color_value = strtol( [hexColor UTF8String], (char **)NULL, 16 );
	unsigned char red = (color_value >> 16) & 0xFF;
	unsigned char green = (color_value >> 8) & 0xFF;
	unsigned char blue = color_value & 0xFF;
	
	UIColor *theColor = [ UIColor colorWithRed:(red/255.0f) green:(green/255.0f) blue:(blue/255.0f) alpha:1.0 ];
	return theColor;
}

/**
 *  parse the string returned, of the style: "toolChanged\rsomeTool" find first return, take substring from that index
 *  @param string incoming string with color from Photoshop
 *  @return NSColor representation of the color
 */
- (UIColor*)colorFromResponseString:(NSString*)string
{
	NSRange range = [string rangeOfString:@"\r" ];
	if (range.length == 0)
		return nil;
	
	NSString *hexColor = [string substringFromIndex:(range.location + 1)];
	return [self colorFromString:hexColor];
}



@end
