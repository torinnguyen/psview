//
//  main.m
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TNAppDelegate.h"

int main(int argc, char *argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([TNAppDelegate class]));
  }
}
