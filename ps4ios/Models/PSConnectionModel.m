//
//  PSConnectionModel.m
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import "PSConnectionModel.h"

@implementation PSConnectionModel

@synthesize serverAddress;
@synthesize serverPassword;
@synthesize serviceObject;

@synthesize favorite;
@synthesize lastSeen;
@synthesize lastUsed;
@synthesize lastConnected;

@end
