//
//  PSConnectionModel.h
//  ps4ios
//
//  Created by Torin Nguyen on 23/6/12.
//  Copyright (c) 2012 torinnguyen@gmail.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PSConnectionModel : NSObject

@property (nonatomic, strong) NSString *serverAddress;
@property (nonatomic, strong) NSString *serverPassword;
@property (nonatomic, strong) id serviceObject;

@property (nonatomic, assign) BOOL favorite;
@property (nonatomic, strong) NSDate *lastSeen;
@property (nonatomic, strong) NSDate *lastUsed;
@property (nonatomic, strong) NSDate *lastConnected;

@end
